#!/usr/bin/ruby -Ku

require 'etc'
require 'webrick'
require 'RMagick'
require 'rubygems'
require 'json'
require 'nokogiri'
require 'syslog'
require "#{File.dirname(__FILE__)}/lib/rodeo.rb"
require 'pp'

# DB/アプリ名
TITLE             = "電子化rvng"
DB                = "shokotan"
APP_NAME          = "rvng"
SERVER_ROOT       = "/var/www/shokotan"
SERVER_URL        = "http://dlsv.sgmail.jp"
SERVER_PORT       = 10080
APACHE_USER       = "www-data"
APACHE_URL        = "http://dlsv.sgmail.jp/shokotan"
ERB_FILE          = "./#{File.basename(__FILE__)}"
# テーマ設定
MAIN_THEME        = "b"             # 主テーマ
SLAVE_THEME       = "d"             # 別窓のテーマ
SUB_THEME         = "c"             # ダイアログなどのテーマ
SHELF_THEME       = "c"             # 書籍一覧のテーマ
BTN_OPACITY       = "0.10"          # viewer操作ボタンの透明度
SLIDER_HEIGHT     = 0               # ページスライダーの高さ(未実装)
TITLEBAR_SIZE     = 40              # タイトルバーの高さ
# 一覧
THUMBNAIL_WIDTH   = 150             # サムネイル横幅
THUMBNAIL_HEIGHT  = 240             # サムネイル高さ
THUMBNAIL_MOBILE  = 0.50            # モバイルの時のサムネイルの表示倍率
THUMBNAIL_PC      = 0.75            # PCの時のサムネイルの表示倍率
# 画像生成
USEGS_EXTRACT     = false           # GSを使ってページを抽出
EXTRACT_DENSITY   = 600             # PDF->JPEG時の解像度
COLOR_FORMAT      = "jpg"           # カラーの画像フォーマット
GRAY_FORMAT       = "jpg"           # グレースケールの画像フォーマット
MONO_FORMAT       = "jpg"           # 白黒の画像フォーマット
HALF_JPEG_QUALITY = 100             # jpegの品質(表示用)
FULL_JPEG_QUALITY = 50              # jpegの品質(拡大用)
COLOR_DEPTH       = 16              # カラーページ色深度
GRAY_DEPTH        = 8               # グレー・白黒色深度
MONO_DEPTH        = 8               # グレー・白黒色深度
DESKEW_PARAM      = 0.7             # 傾き補正のしきい値
DESKEW_LIMIT      = 0.05            # 傾き補正エラーの判定値
SHARPEN_RADIUS    = 0               # シャープネスの半径
MAGNIFY           = 4.0             # jpegの表示倍率
PCMAGNIFY         = 1.0             # jpegの表示倍率
# ルーペ
PC_LOUPE_SIZE     = 400             # ルーペの大きさ(PC)
MOBILE_LOUPE_SIZE = 300             # ルーペの大きさ(MOBILE)
LOUPE_RADIUS      = 100             # ルーペのコーナー半径
# ページ遷移効果
LOGIN_EFFECT      = "flip"          # ログイン時の遷移
INDEX_EFFECT      = "slide"         # 書籍一覧への遷移
VIEWER_EFFECT     = "flow"          # viewerへの遷移
VIEWER_DIRECTION  = "false"         # viewerのtransitionの方向
PAGE_EFFECT       = "slide"         # ページめくり
# Twitter,Facebook,amazon
TWITTER_ID        = "@densika_com"  # Twitter-id
AMAZON_ASSOCID    = "ashigi-22"     # amazon associate id

# pdf->jpeg変換(単一ページ)

def rmpdf2jpeg(rodeo,server,req,mark,page,pages)
  temp   = "./upload/pdf/#{rodeo['userid']}/viewer"
  width  = rodeo['width'].to_i
  height = rodeo['height'].to_i
  image  = nil
  if(USEGS_EXTRACT != true)
    if(!mark)
      # jpegファイル抽出
      `pdfimages -q -j -f #{page} -l #{page} #{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf #{temp}/rm#{rodeo["roid"]}`
      # server.logger.info("extract #{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page}]")
      # JPEGファイルの読み込み
      list = []
      Dir.foreach("#{temp}") {|file|
        next if(file =~ /^\.+$/)
        case file
        when /^rm#{rodeo["roid"]}\-.*\.jpg$/,/^rm#{rodeo["roid"]}\-.*\.pbm$/,/^rm#{rodeo["roid"]}\-.*\.ppm$/
          list << "#{temp}/#{file}" if(File.size("#{temp}/#{file}") > 1024 * 10)
        when /\.hst$/,/\.pdf$/
        else
          #File.unlink("#{temp}/#{file}")
        end
      }
      list.sort!
      list.each_index {|i|
        image = Magick::Image.read(list[i])[0] if(i < 1)
        #File.unlink(list[i])
      }
    else
      cache = "#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.cache"
      pstr = format("%03d",page - 1)
      if(File.exist?("#{cache}/pg-#{pstr}.jpg"))
        image = Magick::Image.read("#{cache}/pg-#{pstr}.jpg")[0]
      end
    end
  else
    image = Magick::ImageList.new("#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page-1}]"){
      self.density = EXTRACT_DENSITY
    }[0]
  end
  # 自動回転処理
  if(image.columns >= image.rows)
    if(rodeo['rotate'] == "true")
      if((page % 2) == 0)
        image.rotate!(-90)
      else
        image.rotate!( 90)
      end
    else
      if((page % 2) == 0)
        image.rotate!( 90)
      else
        image.rotate!(-90)
      end
    end
  end
  # とりあえず縮小
  image = image.resize_to_fit!(width * 1.2,height * 1.2)
  # deskew
  if(rodeo['deskew'] == "true")
    trim = rodeo['trim'].to_i
    trim = 1 if(rodeo['trim'].to_i == 0)
    cx = (image.columns * (trim.to_f / 100.0) / 2.0).to_i
    cy = (image.rows    * (trim.to_f / 100.0) / 2.0).to_i
    image.crop!(cx,cy,image.columns - cx * 2,image.rows - cy * 2)
    deskew = image.deskew(DESKEW_PARAM)
    # 傾き過ぎは誤りとする
    ratio = (deskew.columns - image.columns).abs.to_f / image.columns.to_f
    if(ratio < DESKEW_LIMIT)
      # 再度大きさも微調整
      if(SHARPEN_RADIUS > 0)
        image = deskew.sharpen(SHARPEN_RADIUS,1.0)
        deskew.destroy!
      else
        image.destroy!
        image = deskew
      end
    end
  else
    # crop
    cx = (image.columns * (rodeo['trim'].to_f / 100.0) / 2.0).to_i
    cy = (image.rows    * (rodeo['trim'].to_f / 100.0) / 2.0).to_i
    image.crop!(cx,cy,image.columns - cx * 2,image.rows - cy * 2) if(rodeo['trim'].to_i != 0)
  end
=begin
  # 黄ばみ除去
  case rodeo['color']
  when /^mono/,/^gray/
    if(page > 1)
      image.fuzz = 50
      opaq = image.opaque('#f9eac6','white')
      image.destroy!
      image = opaq
    end
  end
=end
  # コントラスト調整
  mid = nil
  gma = 1.0
  thd = 40000
  case rodeo['color']
  when /^mono/
    case rodeo['contrast']
    when "1" then thd = 45000
    when "2" then thd = 50000
    when "3" then thd = 55000
    when "4" then thd = 60000
    when "5" then thd = 65000
    end
  when /^gray/
    case rodeo['contrast']
    when "1" then mid = Magick::QuantumRange * 0.95; gma = 0.4
    when "2" then mid = Magick::QuantumRange * 0.85; gma = 0.3
    when "3" then mid = Magick::QuantumRange * 0.75; gma = 0.2
    when "4" then mid = Magick::QuantumRange * 0.70; gma = 0.1
    when "5" then mid = Magick::QuantumRange * 0.65; gma = 0.05
    end
  else
    case rodeo['contrast']
    when "1" then gma = 0.50
    when "2" then gma = 0.75
    when "3" then gma = 1.00
    when "4" then gma = 1.25
    when "5" then gma = 1.50
    end
  end
  # 表紙ページはコントラスト調整なし
  if((page != 1 and page != pages) and mid != nil)
    if(rodeo['color'] !~ /^mono/)
      gamma = image.level(0,mid,gma)
      image.destroy!
      image = gamma
    end
  end
  # チャネル抽出
  case rodeo['color']
  when /^mono/
    if(page > 1 and page < pages)
      red = image.channel(Magick::RedChannel)
      image.destroy!
      image = red.threshold(thd)
    end
  when /^gray/
    if(page > 1 and page < pages)
      red = image.channel(Magick::RedChannel)
      image.destroy!
      image = red
    end
  end
  # カラーモード
  color_mode = nil
  color_depth = nil
  image_format = nil
  case rodeo['color']
  when /^mono/
    if(page > 1 and page < pages)
      color_mode = Magick::BilevelType
      color_depth = MONO_DEPTH
      image_format = MONO_FORMAT
    else
      color_mode = Magick::TrueColorType
      color_depth = COLOR_DEPTH
      image_format = COLOR_FORMAT
    end
  when /^gray/
    if(page > 1 and page < pages)
      color_mode = Magick::GrayscaleType
      color_depth = GRAY_DEPTH
      image_format = GRAY_FORMAT
    else
      color_mode = Magick::TrueColorType
      color_depth = COLOR_DEPTH
      image_format = COLOR_FORMAT
    end
  else
    color_mode = Magick::TrueColorType
    color_depth = COLOR_DEPTH
    image_format = COLOR_FORMAT
  end
  quality = FULL_JPEG_QUALITY
  if(req['user-agent'] =~ /iphone/i or req['user-agent'] =~ /ipad/)
    quality = HALF_JPEG_QUALITY
  end
  # フルサイズの出力
=begin
  full = image.resize_to_fit(width,height)
  full.format = image_format
  full.write("jpeg:#{temp}/rm#{rodeo["roid"]}f.#{image_format}") {
    self.quality = quality
    self.image_type = color_mode
    self.depth = color_depth
  }
=end
  # ハーフサイズの出力
#  half = image.resize_to_fit(full.columns / MAGNIFY,full.rows / MAGNIFY)
  half = image.resize_to_fit((width * rodeo['magnify'].to_f).to_i,(height * rodeo['magnify'].to_f).to_i)
  half.format = image_format
#  half.write("jpeg:#{temp}/rm#{rodeo["roid"]}h.#{image_format}") {
  half.write("#{image_format}:#{temp}/rm#{rodeo["roid"]}h.#{image_format}") {
    self.quality = HALF_JPEG_QUALITY
    self.image_type = color_mode
    self.depth = color_depth
  }
#  x = full.columns
#  y = full.rows
  x = half.columns
  y = half.rows
  image.destroy!
#  full.destroy!
  half.destroy!
  # server.logger.info("create rm#{rodeo["roid"]}.jpg #{Time.now - start}s")
  [x,y,"#{temp}/rm#{rodeo["roid"]}.#{image_format}"]
end

# pdf->jpeg変換(見開き)

def rmpdf2jpeg2(rodeo,server,req,mark,page,pages)
  temp   = "./upload/pdf/#{rodeo['userid']}/viewer"
  width  = rodeo['width'].to_i
  height = rodeo['height'].to_i
  image  = Magick::ImageList.new()
  full   = Magick::ImageList.new()
  half   = Magick::ImageList.new()
  fout   = Magick::ImageList.new()
  hout   = Magick::ImageList.new()
  if(USEGS_EXTRACT != true)
    if(!mark)
      # jpeg抽出(page 1 or 2)
      if((page + 1) <= rodeo['pages'].to_i)
        `pdfimages -q -j -f #{page} -l #{page+1} #{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf #{temp}/rm#{rodeo["roid"]}`
        # server.logger.info("extract #{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page},#{page+1}]")
      else
        `pdfimages -q -j -f #{page} -l #{page+0} #{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf #{temp}/rm#{rodeo["roid"]}`
        # server.logger.info("extract #{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page}]")
      end
      # JPEGファイルの読み込み
      list = []
      Dir.foreach("#{temp}") {|file|
        next if(file =~ /^\.+$/)
        case file
        when /^rm#{rodeo["roid"]}\-.*\.jpg$/,/^rm#{rodeo["roid"]}\-.*\.pbm$/,/^rm#{rodeo["roid"]}\-.*\.ppm$/
          list << "#{temp}/#{file}" if(File.size("#{temp}/#{file}") > 1024 * 10)
        when /\.hst$/,/\.pdf$/
        else
          #File.unlink("#{temp}/#{file}")
        end
      }
      list.sort!
      list.each_index {|i|
        image << Magick::Image.read(list[i])[0] if(i < 2)
        #File.unlink(list[i]) if(File.exist?(list[i]))
      }
    else
      cache = "#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.cache"
      if((page + 1) <= rodeo['pages'].to_i)
        pstr = format("%03d",page - 1)
        if(File.exist?("#{cache}/pg-#{pstr}.jpg"))
          image << Magick::Image.read("#{cache}/pg-#{pstr}.jpg")[0]
        else
          image << Magick::Image.new(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT) { self.background_color = "white" }
        end
        pstr = format("%03d",page + 0)
        if(File.exist?("#{cache}/pg-#{pstr}.jpg"))
          image << Magick::Image.read("#{cache}/pg-#{pstr}.jpg")[0]
        else
          image << Magick::Image.new(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT) { self.background_color = "white" }
        end
      else
        pstr = format("%03d",page - 1)
        if(File.exist?("#{cache}/pg-#{pstr}.jpg"))
          image << Magick::Image.read("#{cache}/pg-#{pstr}.jpg")[0]
        else
          image << Magick::Image.new(THUMBNAIL_WIDTH,THUMBNAIL_HEIGHT) { self.background_color = "white" }
        end
      end
    end
  else
    if((page + 1) <= rodeo['pages'].to_i)
      image << Magick::ImageList.new("#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page-1}]"){
        self.density = EXTRACT_DENSITY
        }[0]
      image << Magick::ImageList.new("#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page+0}]"){
        self.density = EXTRACT_DENSITY
      }[0]
    else
      image << Magick::ImageList.new("#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf[#{page-1}]"){
        self.density = EXTRACT_DENSITY
      }[0]
    end
  end
  # 空白ページの補完
  if(image[1] == nil)
    image[1] = Magick::Image.new(image[0].columns,image[0].rows) { self.background_color = "white" }
  end
  #
  swidth = rodeo['swidth'].to_i
  image.each_index {|i|
    if(image[i].columns >= image[i].rows)
      if(rodeo['rotate'] == "true")
        if(((page + i) % 2) == 0)
          image[i].rotate!(-90)
        else
          image[i].rotate!( 90)
        end
      else
        if(((page + i) % 2) == 0)
          image[i].rotate!( 90)
        else
          image[i].rotate!(-90)
        end
      end
    end
    # とりあえず縮小
    image[i].resize_to_fit!(width * 1.2,height * 1.2)
    # deskew
    if(rodeo['deskew'] == "true")
      # crop
      trim = rodeo['trim'].to_i
      trim = 1 if(rodeo['trim'].to_i == 0)
      cx = (image[i].columns * (trim.to_f / 100.0) / 2.0).to_i
      cy = (image[i].rows    * (trim.to_f / 100.0) / 2.0).to_i
      image[i].crop!(cx,cy,image[i].columns - cx * 2,image[i].rows - cy * 2)
      # deskew
      deskew = image[i].deskew(DESKEW_PARAM)
      # 傾き過ぎは誤りとする
      ratio = (deskew.columns - image[i].columns).abs.to_f / image[i].columns.to_f
      if(ratio < DESKEW_LIMIT)
        # シャープネス
        if(SHARPEN_RADIUS > 0)
          image[i].destroy!
          image[i] = deskew.sharpen(SHARPEN_RADIUS,1.0)
          deskew.destroy!
        else
          image[i].destroy!
          image[i] = deskew
        end
      end
    else
      # crop
      cx = (image[i].columns * (rodeo['trim'].to_f / 100.0) / 2.0).to_i
      cy = (image[i].rows    * (rodeo['trim'].to_f / 100.0) / 2.0).to_i
      image[i].crop!(cx,cy,image[i].columns - cx * 2,image[i].rows - cy * 2) if(rodeo['trim'].to_i != 0)
    end
=begin
    # 黄ばみ除去
    case rodeo['color']
    when /^mono/,/^gray/
      if(page > 1)
        image[i].fuzz = 90
        opaq = image[i].opaque('#f9eac6','white')
        image[i].destroy!
        image[i] = opaq
      end
    end
=end
    # コントラスト調整
    mid = nil
    gma = 1.0
    thd = 40000
    case rodeo['color']
    when /^mono/
      case rodeo['contrast']
      when "1" then thd = 45000
      when "2" then thd = 50000
      when "3" then thd = 55000
      when "4" then thd = 60000
      when "5" then thd = 65000
      end
    when /^gray/
      case rodeo['contrast']
      when "1" then mid = Magick::QuantumRange * 0.95; gma = 0.4
      when "2" then mid = Magick::QuantumRange * 0.85; gma = 0.3
      when "3" then mid = Magick::QuantumRange * 0.75; gma = 0.2
      when "4" then mid = Magick::QuantumRange * 0.70; gma = 0.1
      when "5" then mid = Magick::QuantumRange * 0.65; gma = 0.05
      end
    else
      case rodeo['contrast']
      when "1" then gma = 0.50
      when "2" then gma = 0.75
      when "3" then gma = 1.00
      when "4" then gma = 1.25
      when "5" then gma = 1.50
      end
    end
    # 表紙ページはコントラスト調整なし
    if((page + i) != 1 and (page + i) != pages and mid != nil)
      if(rodeo['color'] !~ /^mono/)
        gamma = image[i].level(0,mid,gma)
        image[i].destroy!
        image[i] = gamma
      end
    end
    # チャネル抽出
    case rodeo['color']
    when /^mono/
      if(page > 1 and page < pages)
        red = image[i].channel(Magick::RedChannel)
        image[i].destroy!
        image[i] = red.threshold(thd)
        # image[i] = red.quantize(MONO_DEPTH,Magick::GRAYColorspace).threshold(thd)
      end
    when /^gray/
      if(page > 1 and page < pages)
        red = image[i].channel(Magick::RedChannel)
        image[i].destroy!
        image[i] = red
      end
    end
    # フルサイズ生成
=begin
    full[i] = image[i].resize_to_fit(width,height)
    if(full[i].columns > (swidth - 2) / 2)
      full[i] = image[i].resize_to_fit((swidth - 2) / 2,20000)
    end
=end
    # ハーフサイズ生成
    half[i] = image[i].resize_to_fit((width * rodeo['magnify'].to_f / 2).to_i,(height * rodeo['magnify'].to_f).to_i)
    if(half[i].columns > (((swidth * rodeo['magnify'].to_f) - 2) / 2).to_i)
      half[i] = image[i].resize_to_fit((((swidth * rodeo['magnify'].to_f) - 2) / 2).to_i,10000)
    end
    image[i].destroy!
  }
  # セパレータ作成
#  full_sep = Magick::Image.new(2,full[0].rows) { self.background_color = "gray" }
  half_sep = Magick::Image.new(1,half[0].rows) { self.background_color = "gray" }
  # 画像の結合
  if(rodeo['mode'] =~ /^yoko/)
=begin
    fout[0] = full[0]
    fout[1] = full_sep
    fout[2] = full[1]
=end
    hout[0] = half[0]
    hout[1] = half_sep
    hout[2] = half[1]
  else
=begin
    fout[0] = full[1]
    fout[1] = full_sep
    fout[2] = full[0]
=end
    hout[0] = half[1]
    hout[1] = half_sep
    hout[2] = half[0]
  end
#  fullimg = fout.append(false)
  halfimg = hout.append(false)
  # カラーモード
  color_mode = nil
  color_depth = nil
  case rodeo['color']
  when /^mono/
    if(page > 1 and page < pages)
      color_mode = Magick::BilevelType
      color_depth = MONO_DEPTH
      image_format = MONO_FORMAT
    else
      color_mode = Magick::TrueColorType
      color_depth = COLOR_DEPTH
      image_format = COLOR_FORMAT
    end
  when /^gray/
    if(page > 1 and page < pages)
      color_mode = Magick::GrayscaleType
      color_depth = GRAY_DEPTH
      image_format = GRAY_FORMAT
    else
      color_mode = Magick::TrueColorType
      color_depth = COLOR_DEPTH
      image_format = COLOR_FORMAT
    end
  else
    color_mode = Magick::TrueColorType
    color_depth = COLOR_DEPTH
    image_format = COLOR_FORMAT
  end
  quality = FULL_JPEG_QUALITY
  if(req['user-agent'] =~ /iphone/i or req['user-agent'] =~ /ipad/)
    quality = HALF_JPEG_QUALITY
  end
  # フルサイズ出力
=begin
  fullimg.format = image_format
  fullimg.write("#{temp}/rm#{rodeo["roid"]}f.#{image_format}") {
    self.quality = quality
    self.image_type = color_mode
    self.depth = color_depth
  }
=end
  # ハーフサイズ出力
  halfimg.format = image_format
#  halfimg.write("#{temp}/rm#{rodeo["roid"]}h.#{image_format}") {
  halfimg.write("#{image_format}:#{temp}/rm#{rodeo["roid"]}h.#{image_format}") {
    self.quality = HALF_JPEG_QUALITY
    self.image_type = color_mode
    self.depth = color_depth
  }
=begin
  x = full[0].columns
  y = full[0].rows
=end
  x = half[0].columns
  y = half[0].rows
  3.times {|i|
#    fout[i].destroy!
    hout[i].destroy!
  }
#  fullimg.destroy!
  halfimg.destroy!
  # server.logger.info("create rm#{rodeo["roid"]}.jpg #{Time.now - start}s")
  [x,y,"#{temp}/rm#{rodeo["roid"]}.#{image_format}"]
end

# invalidレスポンス

def invalid(rodeo,server,req,res)
  resp = {
    "result" => false,
  }
  res.content_type = "text/javascript+json"
  res.body = JSON.dump(resp)
end

# validレスポンス

def valid(rodeo,server,req,res,resp)
  user = nil
  session = nil
  req.cookies.each {|cc|
    case cc.name
    when 'user'    then user = cc.value
    when 'session' then session = cc.value
    end
  }
  users = rodeo.table_new("ユーザ")
  ttl = users['アカウント'].conf['session_ttl'].to_i
  cookie1 = WEBrick::Cookie.new('user'   ,user);    cookie1.expires = Time.now + ttl
  cookie2 = WEBrick::Cookie.new('session',session); cookie2.expires = Time.now + ttl
  res.cookies << cookie1
  res.cookies << cookie2
  res.content_type = "text/javascript+json"
  res.body = JSON.dump(resp)
end

# ログイン処理

def login(rodeo,server,req,res)
  users = rodeo.table_new("ユーザ")
  ttl = users['アカウント'].conf['session_ttl'].to_i
  session = users['アカウント'].login(rodeo['userid'],rodeo['passwd'])
  if(session != nil and users['使用停止'].value != true and (users['有効期限'].value == nil or users['有効期限'].value > Time.now))
    cookie1 = WEBrick::Cookie.new('user',rodeo['userid']); cookie1.expires = Time.now + ttl
    cookie2 = WEBrick::Cookie.new('session',session);      cookie2.expires = Time.now + ttl
    res.cookies << cookie1
    res.cookies << cookie2
    resp = {
      "result"  => true,
      "user"    => rodeo['userid'],
      "session" => session,
    }
    server.logger.info("user: #{rodeo['userid']} login")
  else
    resp = {
      "result"  => false,
    }
  end
  res.content_type = "text/javascript+json"
  res.body = JSON.dump(resp)
end

#
# リジューム
#

def resume(rodeo,server,req,res,users,user)
  if(users['使用停止'].value != true and (users['有効期限'].value == nil or users['有効期限'].value > Time.now))
    server.logger.info("user: #{user} resumed")
    rodeo['table'] = user
    valid(rodeo,server,req,res,{"result"=>true,"user"=>user})
  else
    invalid(rodeo,server,req,res)
  end
end

#
# ログアウト
#

def logout(rodeo,server,req,res)
  user = nil
  session = nil
  req.cookies.each {|cc|
    case cc.name
    when 'user'    then user = cc.value
    when 'session' then session = cc.value
    end
  }
  users = rodeo.table_new("ユーザ")
  cookie1 = WEBrick::Cookie.new('user'   ,user);    cookie1.expires = Time.now - 3000
  cookie2 = WEBrick::Cookie.new('session',session); cookie2.expires = Time.now - 3000
  res.cookies << cookie1
  res.cookies << cookie2
  server.logger.info("user: #{user} logout")
  resp = {
    "result" => true,
  }
  res.content_type = "text/javascript+json"
  res.body = JSON.dump(resp)
end

#
# パスワード変更
#

def passwd(rodeo,server,req,res,users,user)
  roid = users.find("アカウント_0",user)
  if(roid)
    users['アカウント'].passwd.set_passwd(rodeo['passwd'])
    users.update(roid)
    resp = {
      "result" => true,
      "passwd" => rodeo['passwd'],
    }
    valid(rodeo,server,req,res,resp)
  else
    invalid(rodeo,server,req,res)
  end
end

# テーブル一覧

def table(rodeo,server,req,res)
  tables = []
  rodeo.sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|qres|
    qres.each {|ent|
      name = ent['tablename'].sub(/^rc_/,"")
      table = rodeo.table_new(name)
      count = table.count()
      tables << {
        "name"  => name,
        "count" => count,
      }
    }
  }
  resp = {
    "result"  => true,
    "tables"  => tables,
  }
  valid(rodeo,server,req,res,resp)
end

# レコード一覧

def record(rodeo,server,req,res)
#  start = Time.now
  return invalid(rodeo,server,req,res) if(rodeo['table'] == "null")
  table = rodeo.table_new(rodeo['table'])
  title = table.farray[1]['name']
  records = []
  offset = rodeo['offset'].to_i
  # 検索条件
  # マーク
  mark = nil
  case rodeo['mark']
  when "on"  then mark = %Q[("roid_7" = true)]
  when "off" then mark = %Q[("roid_7" = false or "roid_7" is NULL)]
  end
  # where
  expr = ""
  opr = "or"
  if(rodeo['where'] != "")
    wstr = rodeo['where']
    if(wstr =~ /^&[\s　]*(.*?)[\s　]*$/)
      opr = "and"
      wstr = $1
    end
    wstr.split(/[,\s、，　]/).each {|w|
      if(w =~ /^(.*?)\(.*?\)$/)
        w = $1
      end
      case table.fields[title]['type']
      when "RFpdfupload"
        expr << %Q[ #{opr} ("書名" ~* '#{w}')]
      else
        expr << %Q[ #{opr} ("#{title}" ~* '#{w}')]
      end
      if(opr == "or")
        table.farray.each_index {|i|
          case table.farray[i]['name']
          when "出版社","著者","翻訳他"
            case table.farray[i]['type']
            when "RFamazon_keyword","RFselect"
              expr << %Q[ #{opr} ("#{table.farray[i]['name']}"[1] in (select "roid" from "rd_#{table.farray[i]['fobj'].conf['master_table']}" where replace(replace("#{table.farray[i]['fobj'].name}",'　',''),' ','') ~* '#{w}'))]
            when "RFstring"
              expr << %Q[ #{opr} ("#{table.farray[i]['name']}" ~* '#{w}']
            end
          end
        }
      end
    }
    expr.sub!(/^ #{opr} \(/,"(")
  end
  if(mark != nil)
    if(expr != "")
      where = %Q[#{mark} and (#{expr})]
    else
      where = %Q[#{mark}]
    end
  else
    if(expr != "")
      where = %Q[#{expr}]
    else
      where = nil
    end
  end
  # 並び替え
  order = nil
  case rodeo['order']
  when "roid"
    case rodeo['dir']
    when "asc"
      order = %Q("roid" asc)
    else
      order = %Q("roid" desc)
    end
  else
    case rodeo['dir']
    when "asc"
      # PDFファイル型の時の例外処理
      if(table.fields[title]['type'] == "RFpdfupload")
        # order = %Q(replace(replace(upper(substring("#{title}" from '[^/]*$')),'　',''),' ','') asc)
        order = %Q("書名" asc)
      else
        order = %Q(replace(replace(upper("#{title}"),'　',''),' ','') asc)
      end
    else
      # PDFファイル型の時の例外処理
      if(table.fields[title]['type'] == "RFpdfupload")
        # order = %Q(replace(replace(upper(substring("#{title}" from '[^/]*$')),'　',''),' ','') desc)
        order = %Q("書名" desc)
      else
        order = %Q(replace(replace(upper("#{title}"),'　',''),' ','') desc)
      end
    end
  end
  count = table.count(where)
  table.fetch(offset,rodeo['count'].to_i,where,order) {|roid|
    text = table[title].to_s()
    # 最終読み込みページ
    page = 0
    max = 0
    if(File.exist?("./upload/pdf/#{rodeo['userid']}/viewer/#{table.name}.#{roid}.hst"))
      File.open("./upload/pdf/#{rodeo['userid']}/viewer/#{table.name}.#{roid}.hst","r") {|fd|
        line = fd.gets(); page = line.to_i if(line)
        line = fd.gets(); rodeo['color']       = line.chomp! if(line)
        line = fd.gets(); rodeo['mode']        = line.chomp! if(line)
        line = fd.gets(); rodeo['contrast']    = line.chomp! if(line)
        line = fd.gets(); rodeo['trim']        = line.chomp! if(line)
        line = fd.gets()
        line = fd.gets()
        line = fd.gets()
        line = fd.gets()
        line = fd.gets(); rodeo['rotate']      = line.chomp! if(line)
        line = fd.gets(); rodeo['effect']      = line.chomp! if(line)
        line = fd.gets(); rodeo['deskew']      = line.chomp! if(line)
        line = fd.gets(); max = line.to_i if(line)
      }
=begin
      if(max == 0)
        open(%Q(|pdfinfo "./upload/pdf/#{rodeo['userid']}/viewer/#{table.name}.#{roid}.pdf")) {|info|
          while(line = info.gets)
            if(line =~ /^Pages:\s+(\d+)$/i)
              max = $1.to_i
            end
          end
        }
      end
=end
    end
    pagestr = ""
    if(page == 0)
      pagestr = "未読"
    else
      if(max != 0)
        case rodeo['mode']
        when "comic","tate","yoko"
          if(page >= (max - 1))
            pagestr = "読了"
          else
            pagestr = "#{page}/#{max}"
          end
        else
          if(page >= (max - 2))
            pagestr = "読了"
          else
            pagestr = "#{page}/#{max}"
          end
        end
      else
        pagestr = "#{page}/---"
      end
    end
#    cover = "./temp/img#{table.name}-#{roid}.jpg"
#    cover = "./images/noimage.jpg" if(not File.exist?(cover))
    cover = "#{APACHE_URL}/temp/img#{table.name}-#{roid}.jpg"
    cover = "#{APACHE_URL}/images/noimage.jpg" if(not File.exist?("./temp/img#{table.name}-#{roid}.jpg"))
    author = nil
    isbn = ""
    subtitle = table["書名"].to_s
    if(table.fields[title]['type'] == "RFpdfupload")
      author = table[title].author if(text !~ /\.pdf$/)
      author = "" if(author == nil)
    end
#=begin
    if(subtitle =~ /^(.*)-(.*?)-\d+p_([\dX]{10,13})\.pdf$/)
      subtitle = $1
      isbn = $3
    else
      if(subtitle =~ /^(.*)\s*-\s*\(著\)(.*?)_([\dX]{10,13})\.pdf$/)
        subtitle = $1
        author = $2
        isbn = $3
      else
        if(subtitle =~ /^(.*)\s*-\s*\(著\)(.*?)\.pdf$/)
          subtitle = $1
          author = $2
        else
          if(subtitle =~ /^(.*)\.pdf$/)
            subtitle = $1
            author = ""
          end
        end
      end
    end
#=end
    # ASINコード
    asin = nil
#=begin
    if(isbn != "")
      begin
        File.open("./upload/amazon/#{isbn}.xml","r") {|fd|
          xml = Nokogiri(fd.read())
          item = xml % "Item"
          # ASIN code
          asin = (item % "ASIN").text if(item)
        }
      rescue
        asin = ""
      end
    end
#=end
    #
    records << {
      "roid"     => roid,
      "title"    => CGI::escapeHTML(text),
      "subtitle" => CGI::escapeHTML(subtitle),
      "author"   => author,
      "distr"    => table["出版社"].to_s,
      "enable"   => table[title].enable(),
      "mark"     => ((table['roid'].mark.value) ? true : false),
      "cover"    => cover,
      "isbn"     => isbn,
      "page"     => pagestr,
      "asin"     => asin,
    }
  }
  resp = {
    "result"  => true,
    "count"   => count,
    "field"   => title,
    "where"   => where,
    "records" => records,
  }
  valid(rodeo,server,req,res,resp)
end

#
# 詳細表示
#

def detail(rodeo,server,req,res)
  table = rodeo.table_new(rodeo['table'])
  table.select(rodeo['roid'])
  title = table.farray[1]['fobj'].to_s()
  details = []
  table.farray.each_index {|i|
    text = table.farray[i]['fobj'].to_s()
    thumb = nil
    href = nil
    # PDFファイル型の時の例外処理
    if(table.farray[i]['type'] == "RFpdfupload")
      text = table.farray[i]['fobj'].title()
      if(text == "")
        text = File.basename(table.farray[i]['fobj'].to_s())
      end
      thumb = "./temp/img#{table.name}-#{rodeo['roid']}.jpg"
      href = %Q(#viewer)
    end
    details << {
      "name"   => table.farray[i]['name'],
      "enable" => table.farray[i]['fobj'].enable(),
      "value"  => text,
      "thumb"  => thumb,
      "href"   => href,
    }
  }
  resp = {
    "result"  => true,
    "title"   => title,
    "mark"    => ((table['roid'].mark.value) ? true : false),
    "details" => details,
  }
  valid(rodeo,server,req,res,resp)
end

#
# マークのON/OFF
#

def mark(rodeo,server,req,res)
  table = rodeo.table_new(rodeo['table'])
  table.select(rodeo['roid'])
  if(rodeo['mark'] == "on")
    table['roid'].mark.value = true
    table.update(rodeo['roid'])
  else
    table['roid'].mark.value = false
    table.update(rodeo['roid'])
    # 展開したファイルを削除
    temp = "./upload/pdf/#{rodeo['userid']}/viewer"
    cache = "#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.cache"
    if(File.exist?(cache))
      Dir.foreach(cache) {|file|
        next if(file =~ /^\.+$/)
        File.unlink("#{cache}/#{file}")
      }
      Dir.rmdir(cache)
    end
  end
  resp = {
    "result"  => true,
  }
  valid(rodeo,server,req,res,resp)
end

# 以前の情報の取得

def extract2cache(rodeo,temp,cache)
  if(not File.exist?(cache))
    Dir.mkdir(cache)
    system("pdfimages -q -j #{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf #{cache}/pg")
    ary = []
    Dir.foreach(cache).sort.each {|ent|
      next if(ent =~ /^\.+$/)
      if(File.size?("#{cache}/#{ent}") > 8)
        ary << ent
      else
        File.unlink("#{cache}/#{ent}")
      end
    }
    ary.each_index {|i|
      dst = "pg-#{format("%03d",i)}.jpg"
      if(ary[i] != dst)
        File.rename("#{cache}/#{ary[i]}","#{cache}/#{dst}")
      end
    }
  end
end

def viewer_resume(rodeo,server,req,res)
  table = rodeo.table_new(rodeo['table'])
  table.select(rodeo['roid'])
  title = table.farray[1]['fobj'].to_s()
  # ワークの設定
  temp = "./upload/pdf/#{rodeo['userid']}/viewer"
  cache = "#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.cache"
  system("mkdir -p #{temp}") if(not File.exist?(temp))
  if(table[rodeo['field']].value =~ /\.pdf$/)
    if(not File.exist?("#{temp}/#{table.name}.#{rodeo['roid']}.pdf"))
      system(%Q(ln -sf "#{SERVER_ROOT}/allbooks#{table[rodeo['field']].value}" "#{temp}/#{table.name}.#{rodeo['roid']}.pdf"))
    end
  end
  # 前の表示情報の取得
  if(File.exist?("#{temp}/#{table.name}.#{rodeo['roid']}.hst"))
    File.open("#{temp}/#{table.name}.#{rodeo['roid']}.hst","r") {|fd|
      page = 0
      max = 0
      line = fd.gets(); page = line.to_i if(line)
      line = fd.gets(); rodeo['color']       = line.chomp! if(line)
      line = fd.gets(); rodeo['mode']        = line.chomp! if(line)
      line = fd.gets(); rodeo['contrast']    = line.chomp! if(line)
      line = fd.gets(); rodeo['trim']        = line.chomp! if(line)
      line = fd.gets()
      line = fd.gets()
      line = fd.gets()
      line = fd.gets()
      line = fd.gets(); rodeo['rotate']      = line.chomp! if(line)
      line = fd.gets(); rodeo['effect']      = line.chomp! if(line)
      line = fd.gets(); rodeo['deskew']      = line.chomp! if(line)
      line = fd.gets(); max = line.to_i if(line)
=begin
      if(max == 0)
        open(%Q(|pdfinfo "#{temp}/#{table.name}.#{rodeo['roid']}.pdf")) {|info|
          while(line = info.gets)
            if(line =~ /^Pages:\s+(\d+)$/i)
              max = $1.to_i
            end
          end
        }
      end
=end
      resp = {
        "result"      => true,
        "title"       => title,
        "pages"       => max,
        "page"        => page,
        "color"       => rodeo['color'],
        "mode"        => rodeo['mode'],
        "contrast"    => rodeo['contrast'],
        "deskew"      => ((rodeo['deskew'] == "true") ? true : false),
        "trim"        => rodeo['trim'],
        "rotate"      => ((rodeo['rotate'] == "true") ? true : false),
        "effect"      => ((rodeo['effect'] == "true") ? true : false),
      }
      extract2cache(rodeo,temp,cache)
      valid(rodeo,server,req,res,resp)
    }
  else
    extract2cache(rodeo,temp,cache)
    invalid(rodeo,server,req,res)
  end
end

#
# viewer
#

def viewer(rodeo,server,req,res)
  table = rodeo.table_new(rodeo['table'])
  table.select(rodeo['roid'])
  title = table.farray[1]['fobj'].to_s()
  # ワークの設定
  temp = "./upload/pdf/#{rodeo['userid']}/viewer"
  # 読み込みページの決定
  page = 1
  if(rodeo['page'].to_i == 0)
    if(File.exist?("#{temp}/#{table.name}.#{rodeo['roid']}.hst"))
      File.open("#{temp}/#{table.name}.#{rodeo['roid']}.hst","r") {|fd|
        line = fd.gets(); page = line.to_i if(line)
      }
    end
  else
    page = rodeo['page'].to_i
  end
  # ページ数の取得
  max = 0
  if(table['ページ数'].value == nil)
    info = open(%Q(|pdfinfo "#{temp}/#{table.name}.#{rodeo['roid']}.pdf")) {|info|
      while(line = info.gets)
        if(line =~ /^Pages:\s+(\d+)$/i)
          max = $1.to_i
          rodeo['pages'] = max;
        end
      end
    }
  else
    max = table['ページ数'].value
    rodeo['pages'] = max;
  end
  # 表示モード
  if(rodeo['mode'] == "")
    rodeo['mode'] = "tate"
  end
  sizes = nil
  offset = 0
  case rodeo['mode']
  when "tate2","yoko2"
    sizes = rmpdf2jpeg2(rodeo,server,req,true,page,max)
    offset = 2
  else
    sizes = rmpdf2jpeg(rodeo,server,req,true,page,max)
    offset = 1
  end
  resp = {
    "result"  => true,
    "title"   => title,
    "pages"   => max,
    "page"    => ((rodeo['page'] == "0") ? page : nil),
    "rotate"  => rodeo['rotate'],
    "width"   => sizes[0],
    "height"  => sizes[1],
    "image"   => "#{sizes[2]}",
  }
  if(rodeo['prefetch'] == "on")
    File.open("#{temp}/#{table.name}.#{rodeo['roid']}.hst","w") {|fd|
      fd.puts "#{page - offset}"
      fd.puts "#{rodeo['color']}"
      fd.puts "#{rodeo['mode']}"
      fd.puts "#{rodeo['contrast']}"
      fd.puts "#{rodeo['trim'].to_i}"
      fd.puts "0"
      fd.puts "0"
      fd.puts "0"
      fd.puts "false"
      fd.puts "#{rodeo['rotate']}"
      fd.puts "#{rodeo['effect']}"
      fd.puts "#{rodeo['deskew']}"
      fd.puts "#{max}"
    }
  end
  valid(rodeo,server,req,res,resp)
end

# page

def pageload(rodeo,server,req,res)
  content_type = nil
  image_format = nil
  res.content_length = 0
  table = rodeo.table_new(rodeo['table'])
  table.select(rodeo['roid'])
  # ワークの設定
  temp = "./upload/pdf/#{rodeo['userid']}/viewer"
  # 読み込みページの決定
  page = 1
  if(rodeo['page'].to_i == 0)
    if(File.exist?("#{temp}/#{table.name}.#{rodeo['roid']}.hst"))
      File.open("#{temp}/#{table.name}.#{rodeo['roid']}.hst","r") {|fd|
        line = fd.gets(); page = line.to_i if(line)
      }
    end
  else
    page = rodeo['page'].to_i
  end
  # ページ数の取得
  max = 0
  if(table['ページ数'].value == nil)
    info = open(%Q(|pdfinfo "#{temp}/#{table.name}.#{rodeo['roid']}.pdf")) {|info|
      while(line = info.gets)
        if(line =~ /^Pages:\s+(\d+)$/i)
          max = $1.to_i
          rodeo['pages'] = max;
        end
      end
    }
  else
    max = table['ページ数'].value
    rodeo['pages'] = max;
  end
  case rodeo['color']
  when /^mono/
    if(page > 1 and page < max)
      image_format = MONO_FORMAT
    else
      image_format = COLOR_FORMAT
    end
  when /^gray/
    if(page > 1 and page < max)
      image_format = GRAY_FORMAT
    else
      image_format = COLOR_FORMAT
    end
  else
    image_format = COLOR_FORMAT
  end
#  file = "./upload/pdf/#{rodeo['userid']}/viewer/rm#{rodeo['roid']}h.jpg"
#  content_type = "image/jpeg"
  file = "./upload/pdf/#{rodeo['userid']}/viewer/rm#{rodeo['roid']}h.#{image_format}"
  content_type = "image/#{image_format}"
  res.content_type = content_type
  begin
    res.content_length = File.size?(file)
    File.open(file,"rb") {|fd| res.body = fd.read() }
  rescue
  end
end

# pdfdownload

def pdfdownload(rodeo,server,req,res,user)
  table = rodeo.table_new(user)
  table.select(rodeo['roid'])
  # ワークの設定
  file = "#{SERVER_ROOT}/allbooks#{table[rodeo['field']].value}"
  if(File.exist?(file))
    name = table[rodeo['field']].value.split(/\//)[-1]
    # res['Date'] = "#{Time.now.gmtime.strftime("%a, %d %b %Y %H:%M:%S GMT")}"
    res['Content-Disposition'] = "inline; filename=\"#{name}\""
    res.content_type = "application/pdf"
    File.open(file,"rb") {|fd| res.body = fd.read() }
  else
    error =<<EOS
<html>
  <body>
    ファイルがありません。
  </body>
</html>
EOS
    # res['Date'] = "#{Time.now.gmtime.strftime("%a, %d %b %Y %H:%M:%S GMT")}"
    res.content_type = "text/html"
    res.body = error
  end
end

# cleanup

def cleanup(rodeo,server,req,res)
  temp = "./upload/pdf/#{rodeo['userid']}/viewer"
  cache = "#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.cache"
  # 読了の場合展開したファイルを削除
  if(File.exist?("#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.hst"))
    File.open("#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.hst","r") {|fd|
      page = 0
      max = 0
      line = fd.gets(); page = line.to_i if(line)
      line = fd.gets(); rodeo['color']       = line.chomp! if(line)
      line = fd.gets(); rodeo['mode']        = line.chomp! if(line)
      line = fd.gets(); rodeo['contrast']    = line.chomp! if(line)
      line = fd.gets(); rodeo['trim']        = line.chomp! if(line)
      line = fd.gets()
      line = fd.gets()
      line = fd.gets()
      line = fd.gets()
      line = fd.gets(); rodeo['rotate']      = line.chomp! if(line)
      line = fd.gets(); rodeo['effect']      = line.chomp! if(line)
      line = fd.gets(); rodeo['deskew']      = line.chomp! if(line)
      line = fd.gets(); max = line.to_i if(line)
      table = rodeo.table_new(rodeo['table'])
      table.select(rodeo['roid'])
      if(table['ページ数'].value == nil)
        if(max == 0)
          open(%Q(|pdfinfo "#{temp}/#{rodeo['userid']}.#{rodeo['roid']}.pdf")) {|info|
            while(line = info.gets)
              if(line =~ /^Pages:\s+(\d+)$/i)
                max = $1.to_i
              end
            end
          }
        end
      else
        max = table['ページ数'].value
      end
      if(page >= (max - 2) or table['roid'].mark.value == false)
        if(File.exist?(cache))
          Dir.foreach(cache) {|file|
            next if(file =~ /^\.+$/)
            File.unlink("#{cache}/#{file}")
          }
          Dir.rmdir(cache)
        end
      end
    }
  end
  file = "./upload/pdf/#{rodeo['userid']}/viewer/rm#{rodeo['roid']}f.jpg"
  File.unlink(file) if(File.exist?(file))
  file = "./upload/pdf/#{rodeo['userid']}/viewer/rm#{rodeo['roid']}h.jpg"
  File.unlink(file) if(File.exist?(file))
  resp = {
    "result"  => true,
  }
  valid(rodeo,server,req,res,resp)
end

# facebook

def facebook(rodeo,server,req,res)
  user = CGI::unescape(rodeo['user'])
  table = rodeo.table_new(user)
  table.select(rodeo['roid'])
  title = table['PDFファイル'].to_s
  url   = "#{SERVER_URL}:#{SERVER_PORT}/#{APP_NAME}?action=facebook&user=#{rodeo['user']}&roid=#{rodeo['roid']}"
  image = "#{SERVER_URL}:#{SERVER_PORT}/#{APP_NAME}?action=fbimage&user=#{rodeo['user']}&roid=#{rodeo['roid']}"
  isbn = nil
  xml = nil
  asin = nil
  author = nil
  if(table['PDFファイル'].value =~ /_([\dX]{10,13}).pdf$/)
    isbn = $1
    # XML code
    File.open("./upload/amazon/#{isbn}.xml","r") {|fd|
      xml = Nokogiri(fd.read())
      item = xml % "Item"
      # ASIN code
      asin = (item % "ASIN").text
      ar = []
      (item / "Author").each {|i| ar << i.text }
      author = ar.join(",")
    }
  end
  Rodeo.clear()
  rodeo.eruby(ERB_FILE,"facebook",binding)
  res.content_type = "text/html"
  res.body = Rodeo.gettext()
end

# facebook image

def fbimage(rodeo,server,req,res)
  image = Magick::ImageList.new()
  file = "./temp/img#{rodeo['user']}-#{rodeo['roid']}.jpg"
  if(File.exist?(file))
    image[1] = Magick::Image.read("./temp/img#{rodeo['user']}-#{rodeo['roid']}.jpg")[0]
    image[0] = Magick::Image.new(35,image[1].rows) { self.background_color = "white" }
    image[2] = Magick::Image.new(35,image[1].rows) { self.background_color = "white" }
    bin = image.append(false).resize_to_fit(403,403).to_blob {
      self.format = "jpeg"
    }
    3.times {|i| image[i].destroy! }
    # res['Date'] = "#{Time.now.gmtime.strftime("%a, %d %b %Y %H:%M:%S GMT")}"
    res.content_type = "image/jpeg"
    res.content_length = bin.size
    res.body = bin
  else
    # res['Date'] = "#{Time.now.gmtime.strftime("%a, %d %b %Y %H:%M:%S GMT")}"
    res.content_type = "image/jpeg"
    res.content_length = 0
  end
end

#
# イベント分岐
#

def dispatch(rodeo,server,req,res)
  start = Time.now()
  rodeo.cgi = {}
  req.query.each {|k,v| rodeo[k] = v }
  case rodeo['action']
  when "login"
    login(rodeo,server,req,res)
  when "facebook"
    facebook(rodeo,server,req,res)
  when "fbimage"
    fbimage(rodeo,server,req,res)
  when "rodeo.js"
    Rodeo.clear()
    rodeo.eruby(ERB_FILE,"rodeo.js",binding)
    res.content_type = "text/javascript"
    res.body = Rodeo.gettext()
  when "jqminit.js"
    Rodeo.clear()
    rodeo.eruby(ERB_FILE,"jqminit.js",binding)
    res.content_type = "text/javascript"
    res.body = Rodeo.gettext()
  when "rodeo.css"
    Rodeo.clear()
    rodeo.eruby(ERB_FILE,"rodeo.css",binding)
    res.content_type = "text/css"
    res.body = Rodeo.gettext()
  else
    Rodeo.clear()
    user = nil
    session = nil
    req.cookies.each {|cc|
      case cc.name
      when 'user'    then user = cc.value
      when 'session' then session = cc.value
      end
    }
    users = rodeo.table_new("ユーザ")
    if(users['アカウント'].valid?(user,session))
      if(user == "admin")
        rodeo['table'] = "allbooks"
        rodeo['userid'] = "allbooks"
      end
      case rodeo['action']
      when "resume"            then resume(rodeo,server,req,res,users,user)
      when "passwd"            then passwd(rodeo,server,req,res,users,user)
      when "logout"            then logout(rodeo,server,req,res)
      when "table"             then table(rodeo,server,req,res)
      when "record"            then record(rodeo,server,req,res)
      when "detail"            then detail(rodeo,server,req,res)
      when "mark"              then mark(rodeo,server,req,res)
      when "viewer_resume"     then viewer_resume(rodeo,server,req,res)
      when "viewer"            then viewer(rodeo,server,req,res)
      when "page"              then pageload(rodeo,server,req,res)
      when "pdfdownload"       then pdfdownload(rodeo,server,req,res,user)
      when "cleanup"           then cleanup(rodeo,server,req,res)
      else
        res.content_type = "text/html"
        rodeo.eruby(ERB_FILE,"main",binding)
        res.body = Rodeo.gettext()
      end
    else
      case rodeo['action']
      when "resume" then invalid(rodeo,server,req,res)
      else
        res.content_type = "text/html"
        rodeo.eruby(ERB_FILE,"main",binding)
        res.body = Rodeo.gettext()
      end
    end
  end
  sec = Time.now - start
  server.logger.info("(#{format("%6.02f",sec)}sec) action : #{rodeo['action']} accepted from #{rodeo['userid']}")
end

#
# Webrickサーバ
#

# サーバ実行

def server_run(logger)
  option = {
    :DocumentRoot        => SERVER_ROOT,
    :BindAddress         => '0.0.0.0',
    :Port                => SERVER_PORT,
    :CGIInterpreter      => "/usr/bin/ruby",
    :DirectoryIndex      => [APP_NAME],
    :DocumentRootOptions => { :FancyIndexing => false },
    :AccessLog           => [[File.open("./#{APP_NAME}.log","w"),WEBrick::AccessLog::CLF]],
  }
  option[:Logger] = logger if(logger)
  server = WEBrick::HTTPServer.new(option)
  server.mount('/image', WEBrick::HTTPServlet::FileHandler,SERVER_ROOT+"/image" )
  server.mount('/images',WEBrick::HTTPServlet::FileHandler,SERVER_ROOT+"/images")
  server.mount('/temp',  WEBrick::HTTPServlet::FileHandler,SERVER_ROOT+"/temp"  )
  server.mount('/upload',WEBrick::HTTPServlet::FileHandler,SERVER_ROOT+"/upload")
  server.mount('/favicon.ico',WEBrick::HTTPServlet::FileHandler,SERVER_ROOT+"/favicon.ico")
  pgsql = Rodeo::SQL.new(DB,"localhost",5432,nil,nil,"www-data",nil)
  rodeo = Rodeo.new(__FILE__,pgsql)
  ["/","/#{APP_NAME}"].each {|cgi|
    server.mount_proc(cgi) {|req,res|
      # rodeo = Rodeo.new(__FILE__,pgsql)
      res['Cache-Control'] = "no-cache"
      res.keep_alive=(false)
      # res.keep_alive=(true)
      # server.logger.info(req.header)
      dispatch(rodeo,server,req,res)
    }
  }
  ['INT','TERM','HUP'].each {|signal|
    Signal.trap(signal) {
      if(server.respond_to?(:shutdown))
        server.shutdown()
      else
        exit
      end
    }
  }
  ent = Etc.getpwnam(APACHE_USER)
  if(ent and ent['uid'])
#    `renice +20 #{Process.pid}`
    Process::UID.change_privilege(ent['uid'])
    Dir.chdir(SERVER_ROOT)
    server.start
  end
end

# サーバ開始

def server_start()
  Process.fork() {
    Process::setsid()
    Process.fork() {
      File.open("/var/run/#{APP_NAME}-webrick.pid","w") {|fd| fd.write(Process.pid) }
      logger = Syslog.open("#{APP_NAME}-webrick")
      server_run(logger)
    }
  }
end

# サーバ停止

def server_stop()
  if(File.exist?("/var/run/#{APP_NAME}-webrick.pid"))
    File.open("/var/run/#{APP_NAME}-webrick.pid","r") {|fd|
      pid = fd.read.to_i
      begin
        Process.kill("HUP",pid) if(pid > 0)
        File.unlink("/var/run/#{APP_NAME}-webrick.pid")
      rescue
      end
    }
  end
end

# サーバ start/stop/restart/debug

case ARGV[0]
when /^debug$/i  then server_run(nil)
when /^start$/i  then server_start()
when /^stop$/i   then server_stop()
when /^restart/i
  server_stop()
  sleep(0.5)
  server_start()
end

__END__

#### rodeo.css

/* gridの拡張 */

/* content configurations. */
.ui-grid-a, .ui-grid-b, .ui-grid-c, .ui-grid-d, .ui-grid-e, .ui-grid-f { overflow: hidden; }
.ui-block-a, .ui-block-b, .ui-block-c, .ui-block-d, .ui-block-e, .ui-block-f, .ui-block-g { margin: 0; padding: 0; border: 0; float: left; min-height:1px;}
.ui-grid-a .ui-btn, .ui-grid-b .ui-btn, .ui-grid-c .ui-btn, .ui-grid-d .ui-btn, .ui-grid-e .ui-btn, .ui-grid-f .ui-btn, .ui-grid-g .ui-btn, .ui-grid-solo .ui-btn { margin-right: 5px; margin-left: 5px; }

/* grid e: 16.6667% */
.ui-grid-e .ui-block-a, .ui-grid-e .ui-block-b, .ui-grid-e .ui-block-c, .ui-grid-e .ui-block-d, .ui-grid-e .ui-block-e, .ui-grid-e .ui-block-f { width: 16.6667%; }
.ui-grid-e > :nth-child(n) { width: 16.6667%; }
.ui-grid-e .ui-block-a { clear: left; }

/* grid f: 14.2857% */
.ui-grid-f .ui-block-a, .ui-grid-f .ui-block-b, .ui-grid-f .ui-block-c, .ui-grid-f .ui-block-d, .ui-grid-f .ui-block-e, .ui-grid-f .ui-block-f, .ui-grid-f .ui-block-g { width: 14.2857%; }
.ui-grid-f > :nth-child(n) { width: 14.2857%; }
.ui-grid-f .ui-block-a { clear: left; }

.ui-icon-newlist {
  background-image: url(./images/newlist.png);
}

.ui-icon-twitter {
  background-image: url(./images/twitter.png);
}

.ui-icon-facebook {
  background-image: url(./images/facebook.png);
}

.ui-icon-amazon {
  background-image: url(./images/amazon.png);
}

/*
.tooltipsy {
    padding: 10px;
    max-width: 200px;
    color: #303030;
    background-color: #f5f5b5;
    border: 1px solid #deca7e;
}
*/

#### jqminit.js

$(document).bind("mobileinit",function() {
  // jQuery Mobile初期設定
  $.mobile.defaultTransition = "none";
  $.mobile.touchOverflowEnabled = true;
  $.mobile.dialog.prototype.options.closeBtnText = "閉じる";
  $.mobile.loadingMessageTextVisible = true;
  $.mobile.loadingMessageTheme = "b";
  // スワイプ検出ピクセル数
  $.event.special.swipe.horizontalDistanceThreshold = 200;
  $.event.special.swipe.durationThreshold = 500;
});

#### rodeo.js

// ------------
// gridの再設定
// ------------

(function($,undefined) {
  $.fn.grid = function(options) {
    return this.each(function() {
      var $this = $(this),
        o = $.extend({
          grid: null
        },options),
        $kids = $this.children(),
        gridCols = {solo:1, a:2, b:3, c:4, d:5, e:6, f:7},
        grid = o.grid,
        iterator;
      if(!grid) {
        if($kids.length <= 7) {
          for(var letter in gridCols) {
            if(gridCols[ letter ] === $kids.length) {
              grid = letter;
            }
          }
        } else {
          grid = "a";
        }
      }
      iterator = gridCols[grid];
      $this.addClass("ui-grid-" + grid);
      $kids.filter(":nth-child(" + iterator + "n+1)").addClass("ui-block-a");
      if(iterator > 1) $kids.filter(":nth-child(" + iterator + "n+2)").addClass("ui-block-b");
      if(iterator > 2) $kids.filter(":nth-child(3n+3)").addClass("ui-block-c");
      if(iterator > 3) $kids.filter(":nth-child(4n+4)").addClass("ui-block-d");
      if(iterator > 4) $kids.filter(":nth-child(5n+5)").addClass("ui-block-e");
      if(iterator > 5) $kids.filter(":nth-child(6n+6)").addClass("ui-block-f");
      if(iterator > 6) $kids.filter(":nth-child(7n+7)").addClass("ui-block-g");
    });
  };
})(jQuery);

// -------------------------
// Rodeoグローバル変数初期化
// -------------------------

var A6_RATIO = 0.63;
var BTN_OPACITY = <%=BTN_OPACITY%>;

// Rodeo本体
var isMobile                = false;
var isIphone                = false;
var isIpad                  = false;
var isAndroid               = false;
var isChrome                = false;
var isFirefox               = false;
var isKindle                = false;
var loginEnable             = true;
var loadRecordList          = true;
var loadPdfImage            = true;
var ajaxTimeout             = 60000;
var searchHistory           = new Array();
var record_menu_obj         = null;
var record_plane            = true;
var record_menu_exec_viewer = false;
var record_menu_exec_loader = false;
var viewer_plane            = true;
var viewer_next_src         = null;
var viewer_next_resp        = null;
var viewer_lock             = false;
var viewer_seq              = 0;
var viewer_resize_timer     = null;
var loupe_display           = false;

// 機種判定
var agent = navigator.userAgent;
if(agent.search(/iPhone/)  != -1) { isMobile  = true; isIphone  = true; }
if(agent.search(/iPad/)    != -1) { isMobile  = true; isIpad    = true; }
if(agent.search(/Android/) != -1) { isMobile  = true; isAndroid = true; }
if(agent.search(/armv7l/)  != -1) { isMobile  = true; isKindle  = true; }
if(agent.search(/Chrome/)  != -1) { isChrome  = true; }
if(agent.search(/Firefox/) != -1) { isFirefox = true; }

// POSTパラメータ
var rodeo = {
  user         : null,      // ユーザ名
  table        : null,      // カレントテーブル
  field        : null,      // カレントフィールド
  count        : 0,         // レコードカウント
  offset       : 0,         // 表示オフセット
  mark         : "all",     // レコードマーク
  where        : "",        // 検索条件
  width        : null,      // 前回のwidth
  height       : null,      // 前回のheight
  order        : "roid",    // 表示順項目
  dir          : "desc",    // 表示順方向
  roid         : null,      // カレントレコード
  hgrid        : 5,         // 横サムネイル個数
  listlen      : 5 * 4,     // １回に表示するレコード件数
  history_size : 20,        // search_historyの上限個数
  title        : null       // 現在の書籍タイトル
}

// pdfviewerパラメータ
var pdfviewer = {
  table        : null,      // 表示中のtable
  field        : null,      // 表示中のfield
  roid         : null,      // 表示中のroid
  page         : (isMobile) ? 1 : 2, // 表示中のpage
  pages        : 0,         // ページ数
  color        : "gray",    // color,gray,mono
  contrast     : "1",       // normal,weak,strong
  mode         : (isMobile) ? "comic" : "tate2",  // comic,tate,tate2,yoko,yoko2
  deskew       : false,     // 自動回転補正
  trim         : 0,         // 余白切り取り
  rotate       : false,     // 逆回転補正
  buttondir    : "normal",  // normal,reverse
  magnify      : (isMobile) ? <%=MAGNIFY%> : <%=PCMAGNIFY%>,               // 表示倍率
  loupe_size   : (isMobile) ? <%=MOBILE_LOUPE_SIZE%> : <%=PC_LOUPE_SIZE%>, // ルーペサイズ
  swipe        : true,      // スワイプでページ送り
  title        : null,      // 書籍タイトル
  usegs        : false      // GSでページを抽出
}

// ------------------
// array -> json 変換
// ------------------

function arrayToJSON(array) {
  var init = true;
  var str = '([';
  for(var i = 0; i < array.length; i ++) {
    if(!init) str += ',';
    str += '"' + array[i].replace('"','\\"','g') + '"';
    init = false;
  }
  str += '])';
  return str;
}

// -----------------
// hash -> json 変換
// -----------------

function hashToJSON(hash) {
  var init = true;
  var str = '({';
  for(var i in hash) {
    if(!init) str += ',';
    str += '"' + i.replace('"','\\"','g') + '":';
    if(typeof(hash[i]) == "string") {
      str += '"' + hash[i].replace('"', '\\"', 'g') + '"';
    } else {
      str += '' + hash[i] + '';
    }
    init = false;
  }
  str += '})';
  return str;
}

// ----------------
// 状態の保存・復旧
// ----------------

// 保存
function store_state() {
  if("<%=rodeo['slave']%>" != "yes") {
    var state = {
      offset : String(rodeo.offset),
      mark   : rodeo.mark,
      dir    : rodeo.dir,
      order  : rodeo.order,
      where  : rodeo.where,
      width  : rodeo.width,
      height : rodeo.height,
      roid   : rodeo.roid,
      title  : rodeo.title,
      field  : rodeo.field
    }
    var json = hashToJSON(state);
    localStorage['rodeo_state'] = json;
  }
}
// 復旧
function load_state() {
  if(localStorage['rodeo_state']) {
    try {
      var array    = eval(localStorage['rodeo_state']);
      rodeo.mark   = array["mark"];
      rodeo.order  = array["order"];
      rodeo.dir    = array["dir"];
      rodeo.offset = Number(array["offset"]);
      rodeo.where  = array["where"];
      rodeo.width  = array["width"];
      rodeo.height = array["height"];
      rodeo.roid   = array["roid"];
      rodeo.title  = array["title"];
      rodeo.field  = array["field"];
    } catch(e) {
    }
  }
}

// ------------------------------
// localStorageに履歴を保存・復旧
// ------------------------------

// 保存
function store_history() {
  var json = arrayToJSON(searchHistory);
  localStorage['search_history'] = json;
}
// 復旧
function load_history() {
  if(localStorage['search_history']) {
    try {
      searchHistory = eval(localStorage['search_history']);
    } catch(e) {
      searchHistory = new Array();
    }
  }
}

// 画像のプリロード
var img = new Image(); img.src = "./images/rodeo-bar.png";
var img = new Image(); img.src = "./images/rodeo-tool.png";
var img = new Image(); img.src = "./images/rodeo-next.png";
var img = new Image(); img.src = "./images/rodeo-prev.png";
var img = new Image(); img.src = "./images/rodeo-null.cur";
var img = new Image(); img.src = "./images/twitter.png";
var img = new Image(); img.src = "./images/facebook.png";
var img = new Image(); img.src = "./images/amazon.png";

// ------------
// jquery初期化
// ------------

$(document).ready(function() {

  // ----------------------
  // ベースウインドウの設定
  // ----------------------

  $(body).css("overflow-x","hidden");
  $(body).css("overflow-y","hidden");
  viewer_setup();

  // ----------------------
  // 右クリックメニュー禁止
  // ----------------------
  
  var userAgent = window.navigator.userAgent.toLowerCase();
  var appVersion = window.navigator.appVersion.toLowerCase();
  if(userAgent.indexOf("msie") != -1 && appVersion.indexOf("msie 10.") != -1) {
  } else {
    /*
    $(document).bind("contextmenu",function(e) {
      if(e.ctrlKey != true) {
        e.preventDefault();
      }
    });
    */
  }
  // -------------------------
  // 強制的にloginページに遷移
  // -------------------------

  if("<%=rodeo['slave']%>" == "yes" || "<%=rodeo['return']%>" == "yes") {
    $.mobile.loadingMessage = "確認中...";
    $.mobile.showPageLoadingMsg();
    var callback = function() {
      var data = {
        action : "resume"
      }
      $.post("<%=APP_NAME%>",data,function(resp) {
        if(resp.result == true) {
          rodeo.user = resp.user;
          if(resp.user == "admin") rodeo.table = "allbooks";
          else                     rodeo.table = resp.user;
          load_state();
          load_history();
          if("<%=rodeo['where']%>" != "") {
            rodeo.offset = 0;
            rodeo.where = "<%=rodeo['where']%>";
            rodeo.order = "name";
            rodeo.dir = "asc";
            rodeo.mark = "all";
          } else {
            if("<%=rodeo['return']%>" != "yes") {
              rodeo.offset = 0;
              rodeo.where = "";
              rodeo.order = "roid";
              rodeo.dir = "desc";
              rodeo.mark = "all";
            }
          }
          // レコード一覧に遷移
          $.mobile.loadingMessage = "読み込み中...";
          var effect = "none";
          $.mobile.showPageLoadingMsg();
          load_recordlist(true,function() {
            $.mobile.changePage("#record_true",{
              transition:effect,
              reverse:false
            });
          });
        } else {
          // ログインに遷移
          $.mobile.changePage("#login",{
            transition:"none",
            reverse:false,
            allowSamePageTransition:true
          });
        }
      });
    };
    setTimeout(callback,1);
  } else {
    var callback = function() {
      $.mobile.loadingMessage = "確認中...";
      $.mobile.showPageLoadingMsg();
      // login済みチェック
      var data = {
        action : "resume"
      }
      $.post("<%=APP_NAME%>",data,function(resp) {
        if(resp.result == true) {
          rodeo.user = resp.user;
          if(resp.user == "admin") rodeo.table = "allbooks";
          else                     rodeo.table = resp.user;
          load_state();
          if(rodeo.roid == null) {
            // login済みの場合
            load_history();
            // レコード一覧に遷移
            $.mobile.loadingMessage = "読み込み中...";
            var effect = "<%=LOGIN_EFFECT%>";
            if("<%=rodeo['slave']%>" == "yes") {
              effect = "none";
            }
            $.mobile.showPageLoadingMsg();
            load_recordlist(true,function() {
              $.mobile.changePage("#record_true",{
                transition:effect,
                reverse:false
              });
            });
          } else {
            // 以前のページを表示
            pdfviewer.title = rodeo.title;
            if($(window).width() < $(window).height()) {
              if(isMobile) {
                pdfviewer.mode = "comic";
              } else {
                pdfviewer.mode = "tate";
              }
            } else {
              pdfviewer.mode = "tate2";
            }
            pdfviewer.page     = 2;
            pdfviewer.contrast = "1";
            pdfviewer.deskew   = true;
            pdfviewer.trim     = 0;
            cursorTimer        = null;
            viewer_resume(function() {
              // ページの読み込み
              load_page(viewer_plane,true,0,function() {
                $.mobile.changePage("#viewer_"+viewer_plane,{
                  transition:"<%=LOGIN_EFFECT%>",
                  reverse:false
                });
              });
            });
          }
        } else {
          // ログインに遷移
          $.mobile.changePage("#login",{
            transition:"none",
            reverse:false,
            allowSamePageTransition:true
          });
        }
      });
    };
    setTimeout(callback,1000);
  }

  // ---------------------
  // loginページ表示前処理
  // ---------------------

  $("#login").bind("pagebeforeshow",function() {
    /*
    // login済みチェック
    var data = {
      action : "resume"
    }
    $.post("<%=APP_NAME%>",data,function(resp) {
      if(resp.result == true) {
        // login済みの場合
        rodeo.user = resp.user;
        rodeo.offset = 0;
        rodeo.where = "";
        if(resp.user == "admin") rodeo.table = "allbooks";
        else                     rodeo.table = resp.user;
        load_state();
        load_history();
        console.log("<%=rodeo['where']%>");
        // 以前の状態に復帰
        if(rodeo.roid == null || "<%=rodeo['slave']%>" == "yes") {
          // スレーブの場合
          if("<%=rodeo['slave']%>" == "yes") {
            rodeo.offset = 0;
            rodeo.mark = "all";
            if("<%=rodeo['where']%>" != "") {
              rodeo.where = "<%=rodeo['where']%>";
              rodeo.order = "name";
              rodeo.dir = "asc";
            } else {
              rodeo.where = "";
              rodeo.order = "roid";
              rodeo.dir = "desc";
            }
          }
          // レコード一覧に遷移
          $.mobile.loadingMessage = "読み込み中...";
          $.mobile.showPageLoadingMsg();
          var effect = "<%=LOGIN_EFFECT%>";
          if("<%=rodeo['slave']%>" == "yes") {
            effect = "none";
          }
          load_recordlist(true,function() {
            $.mobile.changePage("#record_true",{
              transition:effect,
              reverse:false
            });
          });
        } else {
          pdfviewer.title = rodeo.title;
          if($(window).width() < $(window).height()) {
            if(isMobile) {
              pdfviewer.mode = "comic";
            } else {
              pdfviewer.mode = "tate";
            }
          } else {
            pdfviewer.mode = "tate2";
          }
          pdfviewer.page     = 2;
          pdfviewer.contrast = "1";
          pdfviewer.deskew   = true;
          pdfviewer.trim     = 0;
          cursorTimer        = null;
          viewer_resume(function() {
            // ページの読み込み
            load_page(viewer_plane,true,0,function() {
              $.mobile.changePage("#viewer_"+viewer_plane,{
                transition:"<%=LOGIN_EFFECT%>",
                reverse:false
              });
            });
          });
        }
      }
    });
    */
  });

  // ---------------
  // loginページ表示
  // ---------------

  function doScroll() { if (window.pageYOffset === 0) { window.scrollTo(0,1); } }
  $("#login").bind("pageshow",function(e) {
    // $(window).animate({ scrollTop:0 },"fast");
    // setTimeout(function() { $(window).animate({ scrollTop:0 },"fast"); },1000);
    setTimeout(doScroll,100);
  });

  // ---------
  // login処理
  // ---------

  $("#login_button").bind("vclick",function(e) {
    if(loginEnable) {
      // 認証タイムアウトの処理
      var timeout = false;
      var loginTimer = setTimeout(function() {
        timeout = true;
        $.mobile.loadingMessage = "認証タイムアウト";
        $.mobile.showPageLoadingMsg();
        setTimeout(function() {
          $.mobile.hidePageLoadingMsg();
          loginEnable = true;
        },2000);
      },ajaxTimeout);
      // POSTデータ作成
      var data = {
        action : "login",
        userid : $("#userid").val(),
        passwd : $("#passwd").val()
      }
      // 認証中を表示
      $.mobile.loadingMessage = "認証中...";
      $.mobile.showPageLoadingMsg();
      // POSTリクエスト
      $.post("<%=APP_NAME%>",data,function(resp) {
        // ログインタイマーを解除
        clearTimeout(loginTimer);
        if(timeout == false) {
          // 認証結果を記憶
          rodeo.user    = resp.user;
          // localstrageにも記録
          localStorage.setItem("user",rodeo.user);
          // 認証中を非表示
          $.mobile.hidePageLoadingMsg();
          if(resp.result == true) {
            rodeo.offset = 0;
            rodeo.where = "";
            if(resp.user != "admin") {
              rodeo.table = resp.user;
            } else {
              rodeo.table = "allbooks";
            }
            load_state();
            load_history();
            // レコード一覧に遷移
            $.mobile.loadingMessage = "読み込み中...";
            $.mobile.showPageLoadingMsg();
            var effect = "<%=LOGIN_EFFECT%>";
            if("<%=rodeo['slave']%>" == "yes") {
              effect = "none";
            }
            load_recordlist(true,function() {
              $.mobile.hidePageLoadingMsg();
              $.mobile.changePage("#record_true",{
                transition:effect,
                reverse:false
              });
            });
          } else {
            // エラーの時はloadingでメッセージ表示
            loginEnable = false;
            $.mobile.loadingMessage = "認証できませんでした";
            $.mobile.showPageLoadingMsg();
            setTimeout(function() {
              $.mobile.hidePageLoadingMsg();
              loginEnable = true;
            },2000);
          }
        }
      },"json");
    }
    return false;
  });

  // --------------
  // ログアウト処理
  // --------------

  function logout() {
    // POSTデータ作成
    var data = {
      action : "logout",
      userid : rodeo.user
    }
    $.mobile.loadingMessage = "ログアウト...";
    $.mobile.showPageLoadingMsg();
    $.post("<%=APP_NAME%>",data,function(resp) {
      $.mobile.hidePageLoadingMsg();
      $.mobile.changePage("#login",{
        transition:"<%=LOGIN_EFFECT%>",
        reverse:false,
        allowSamePageTransition:true
      });
    });
  };
  $("#logout_button_true,#logout_button_false").bind("vclick",logout);

  // --------------
  // パスワード変更
  // --------------

  $("#usr_config_exec").bind("vclick",function(e) {
    // パスワードをチェック
    var p1 = $("#new_pass1").val();
    var p2 = $("#new_pass2").val();
    if(p1 == p2) {
      if(p1.length >= 6) {
        // POSTデータ作成
        var data = {
          action : "passwd",
          passwd : p1
        }
        $.mobile.loadingMessage = "パスワード変更...";
        $.mobile.showPageLoadingMsg();
        $.post("<%=APP_NAME%>",data,function(resp) {
          $.mobile.hidePageLoadingMsg();
          if(resp.result) {
            $.mobile.loadingMessage = "変更しました";
            $.mobile.showPageLoadingMsg();
            setTimeout(function() {
              $.mobile.hidePageLoadingMsg();
              $.mobile.changePage("#record_"+record_plane,{
                transition:"pop",
                reverse:true
                // changeHash:false
              });
            },2000);
          } else {
            $.mobile.loadingMessage = "変更できませんでした";
            $.mobile.showPageLoadingMsg();
            setTimeout(function() {
              $.mobile.hidePageLoadingMsg();
              $.mobile.changePage("#usr_config",{
                transition:"pop",
                reverse:true,
                changeHash:false
              });
            },5000);
          }
        });
      } else {
        $.mobile.loadingMessage = "パスワードは６文字以上です";
        $.mobile.showPageLoadingMsg();
        setTimeout(function() {
          $.mobile.hidePageLoadingMsg();
          $.mobile.changePage("#usr_config",{
            transition:"pop",
            reverse:true,
            changeHash:false
          });
        },5000);
      }
    } else {
      $.mobile.loadingMessage = "パスワードが一致しません";
      $.mobile.showPageLoadingMsg();
      setTimeout(function() {
        $.mobile.hidePageLoadingMsg();
        $.mobile.changePage("#usr_config",{
          transition:"pop",
          reverse:true,
          changeHash:false
        });
      },5000);
    }
  });

  // ------------
  // markのON/OFF
  // ------------

  function detail_mark(sw) {
    if(sw == true) $.mobile.loadingMessage = "マークO N...";
    else           $.mobile.loadingMessage = "マークOFF...";
    $.mobile.showPageLoadingMsg();
    // POSTデータ作成
    var data = {
      action : "mark",
      userid : rodeo.user,
      table  : rodeo.table,
      roid   : rodeo.roid,
      mark   : ((sw == true) ? "on" : "off")
    }
    // 30秒タイマ
    var viewerTimer = setTimeout(function() {
      timeout = true;
      $.mobile.loadingMessage = "通信エラー";
      $.mobile.showPageLoadingMsg();
      setTimeout(function() {
        $.mobile.hidePageLoadingMsg();
      },2000);
    },ajaxTimeout);
    // POSTリクエスト
    $.post("<%=APP_NAME%>",data,function(resp) {
      // タイマーを解除
      clearTimeout(viewerTimer);
      // 「読み込み中...」の消去
      $.mobile.hidePageLoadingMsg();
    },"json");
  };

  // ----------------
  // viewerの初期設定
  // ----------------

  function viewer_setup() {
    $("#viewer_content_true" ).width($(window).width());
    $("#viewer_content_false").width($(window).width());
    $("#viewer_content_true" ).height($(window).height());
    $("#viewer_content_false").height($(window).height());
    if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
      $("#viewer_content_true" ).css("text-align","left");
      $("#viewer_content_false").css("text-align","left");
    } else {
      if(pdfviewer.mode == "tate2" || pdfviewer.mode == "yoko2") {
        $("#viewer_content_true" ).css("text-align","center");
        $("#viewer_content_false").css("text-align","center");
      } else {
        $("#viewer_content_true" ).css("text-align","center");
        $("#viewer_content_false").css("text-align","center");
      }
    }
    button_setup(true );
    button_setup(false);
  }

  // --------------------
  // カーソル消去タイマー
  // --------------------

  var mouse_move_count = 0;
  var mouse_old_event = null;
  var cursorTimer = null;
  function hide_cursor_timer() {
    $("#viewer_true" ).css("cursor","url('./images/rodeo-null.cur'),default");
    $("#viewer_false").css("cursor","url('./images/rodeo-null.cur'),default");
  }
  function hide_cursor() {
    mouse_move_count = 0;
    cursorTimer = setTimeout(hide_cursor_timer,5000);
  }
  $("#viewer_true,#viewer_false").bind("vmousemove",function(e) {
    if(cursorTimer != null) {
      if(mouse_old_event) {
        var dx,dy;
        dx = Math.abs(e.pageX - mouse_old_event.pageX);
        dy = Math.abs(e.pageY - mouse_old_event.pageY);
        if(dx > 3 || dy > 3) {
          mouse_move_count ++;
        }
      }
      if(mouse_move_count > 10) {
        mouse_move_count = 0;
        $("#viewer_true" ).css("cursor","default");
        $("#viewer_false").css("cursor","default");
        clearTimeout(cursorTimer);
        cursorTimer = setTimeout(hide_cursor_timer,5000);
      }
    }
    mouse_old_event = e;
  });

  // --------------
  // ページローダー
  // --------------

  function load_image(plane,src,resp,width,height,swidth,func) {
    viewer_seq ++;
    
    var half = "<%=APP_NAME%>?action=page&table="+rodeo.table+"&userid="+rodeo.user+"&roid="+rodeo.roid+"&half=h"+"&seq="+viewer_seq;
    // var half = "<%=SERVER_URL%>:<%=SERVER_PORT%>/upload/pdf/"+rodeo.user+"/viewer/rm"+rodeo.roid+"h.jpg"+"?seq="+viewer_seq;
    /*
    if(isIphone || isIpad || isAndroid) {
      half = "<%=APP_NAME%>?action=page&table="+rodeo.table+"&userid="+rodeo.user+"&roid="+rodeo.roid+"&half=h"+"&seq="+viewer_seq;
      // half = "<%=SERVER_URL%>:<%=SERVER_PORT%>/upload/pdf/"+rodeo.user+"/viewer/rm"+rodeo.roid+"f.jpg"+"?seq="+viewer_seq;
    }
    */
    var full = "<%=APP_NAME%>?action=page&table="+rodeo.table+"&userid="+rodeo.user+"&roid="+rodeo.roid+"&half=h"+"&seq="+viewer_seq;
    // var full = "<%=SERVER_URL%>:<%=SERVER_PORT%>/upload/pdf/"+rodeo.user+"/viewer/rm"+rodeo.roid+"f.jpg"+"?seq="+viewer_seq;
    
    // $("<img>").attr("src",full).one("load",function() {
      // if(!isMobile) {
      //   $("#viewer_loupe_"+plane).css("background-image",'url("'+full+'")');
      // }
      $("#viewer_img_"+plane).attr("src",half).one("load",function() {
        func();
        if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
          $("#viewer_img_"+plane).width(resp.width / pdfviewer.magnify);
          $("#viewer_img_"+plane).height(resp.height / pdfviewer.magnify);
        } else {
          if(pdfviewer.mode == "tate2" || pdfviewer.mode == "yoko2") {
            $("#viewer_img_"+plane).width(resp.width / pdfviewer.magnify * 2);
            $("#viewer_img_"+plane).height(resp.height / pdfviewer.magnify);
          } else {
            if(isIphone) {
              $("#viewer_img_"+plane).width(screen.width);
              $("#viewer_img_"+plane).height(height);
            } else {
              $("#viewer_img_"+plane).width(resp.width / pdfviewer.magnify);
              $("#viewer_img_"+plane).height(height);
            }
          }
        }
      });
    // });
  }

  // ------------
  // ページ先読み
  // ------------

  function load_page_next(plane) {
    var offset = (pdfviewer.mode == "tate2" || pdfviewer.mode == "yoko2") ? 2 : 1;
    // 先読み可能？
    if((pdfviewer.page + offset) <= pdfviewer.pages) {
      var width;
      var height;
      // POSTデータ作成
      if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
        height = 10000;
        if(isMobile) {
          width = $("#viewer_content_"+plane).width() - 2;
        } else {
          width = $("#viewer_content_"+plane).width() - 2 - 17;
        }
      } else {
        width = 10000;
        if(isMobile) {
          height = $("#viewer_content_"+plane).height() - 2;
        } else {
          height = $("#viewer_content_"+plane).height() - 2;
        }
      }
      var swidth = $("#viewer_content_"+plane).width() - 1;
      var data = {
        action   : "viewer",
        userid   : rodeo.user,
        table    : rodeo.table,
        field    : rodeo.field,
        roid     : rodeo.roid,
        color    : pdfviewer.color,
        contrast : pdfviewer.contrast,
        mode     : pdfviewer.mode,
        page     : pdfviewer.page + offset,
        deskew   : pdfviewer.deskew,
        prefetch : "on",
        trim     : Number(pdfviewer.trim),
        rotate   : pdfviewer.rotate,
        magnify  : pdfviewer.magnify,
        width    : width,
        height   : height,
        swidth   : swidth
      }
      // 抽出中インジケータ表示
      $("#viewer_loading_"+plane).show();
      $.post("<%=APP_NAME%>",data,function(resp) {
        if(resp.result == true) {
          // 画像読み込み
          viewer_next_resp = resp;
          viewer_next_src = resp.image +'?' + pdfviewer.color + pdfviewer.contrast +
            pdfviewer.mode + width + 'x' + height + 'p' + (pdfviewer.page + offset);
          load_image(!plane,viewer_next_src,viewer_next_resp,width,height,swidth,function() {
            // 抽出中インジケータ消去
            $("#viewer_loading_"+plane).hide();
            setup_opbutton(plane);
          });
          // 「抽出中...」の消去
          $.mobile.hidePageLoadingMsg();
          viewer_lock = false;
        }
      },"json");
    } else {
      setup_opbutton(plane);
      // 「抽出中...」の消去
      $.mobile.hidePageLoadingMsg();
      viewer_lock = false;
    }
  }

  // --------------
  // ページ読み込み
  // --------------

  function load_page(plane,showmsg,delta,func) {
    // ロック中？
    if(!viewer_lock) {
      viewer_lock = true;
      // メッセージ表示
      // if(showmsg) {
        $.mobile.loadingMessage = "ページ抽出中...";
        $.mobile.showPageLoadingMsg();
      // }
      // ページ送り
      pdfviewer.page += delta;
      // 抽出中インジケータの位置
      var offset = 0;
      if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") offset = 17;
      if(pdfviewer.mode == "yoko" || pdfviewer.mode == "yoko2") {
        $("#viewer_loading_"+plane).css("left",($(window).width() - 56 - 10 - offset + 2) + "px");
        $("#viewer_loading_"+plane).css("top" ,($(window).height() - 56 - 10 + 2) + 'px');
      } else {
        $("#viewer_loading_"+plane).css("left",(8 + 10 + 2) + "px");
        $("#viewer_loading_"+plane).css("top" ,($(window).height() - 56 - 10 + 2) + 'px');
      }
      // カーソル消去
      hide_cursor();
      var width;
      var height;
      // POSTデータ作成
      if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
        height = 10000;
        if(isMobile) {
          width = $("#viewer_content_"+plane).width() - 2;
        } else {
          width = $("#viewer_content_"+plane).width() - 2 - 17;
        }
      } else {
        width = 10000;
        if(isMobile) {
          height = $("#viewer_content_"+plane).height() - 2;
        } else {
          height = $("#viewer_content_"+plane).height() - 2;
        }
      }
      var swidth = $("#viewer_content_"+plane).width() - 1;
      var data = {
        action   : "viewer",
        userid   : rodeo.user,
        table    : rodeo.table,
        field    : rodeo.field,
        roid     : rodeo.roid,
        color    : pdfviewer.color,
        contrast : pdfviewer.contrast,
        mode     : pdfviewer.mode,
        page     : pdfviewer.page,
        deskew   : pdfviewer.deskew,
        prefetch : "off",
        trim     : Number(pdfviewer.trim),
        rotate   : pdfviewer.rotate,
        magnify  : pdfviewer.magnify,
        width    : width,
        height   : height,
        swidth   : swidth
      }
      var done = false;
      if(viewer_next_resp) {
        var src = viewer_next_resp.image +'?' + pdfviewer.color + pdfviewer.contrast +
          pdfviewer.mode + width + 'x' + height + 'p' + pdfviewer.page;
        // 先読みがある場合
        if(src == viewer_next_src) {
          load_image(plane,viewer_next_src,viewer_next_resp,width,height,swidth,function() {
            // 「抽出中...」の消去
            $.mobile.hidePageLoadingMsg();
            // 表示したtable,roidを記憶
            pdfviewer.table = rodeo.table;
            pdfviewer.field = rodeo.field;
            pdfviewer.roid = rodeo.roid;
            if(viewer_next_resp.pages != null) pdfviewer.pages = viewer_next_resp.pages;
            if(viewer_next_resp.page != null)  pdfviewer.page  = viewer_next_resp.page;
            func();
            load_page_next(plane);
            viewer_plane = plane;
          });
          done = true;
        }
      }
      if(!done) {
        // POSTリクエスト
        $.post("<%=APP_NAME%>",data,function(resp) {
          if(resp.result == true) {
            // 画像読み込み
            var src = resp.image +'?' + pdfviewer.color + pdfviewer.contrast +
              pdfviewer.mode + width + 'x' + height + 'p' + pdfviewer.page;
            load_image(plane,src,resp,width,height,swidth,function() {
              // 「抽出中...」の消去
              $.mobile.hidePageLoadingMsg();
              // 表示したtable,roidを記憶
              pdfviewer.table = rodeo.table;
              pdfviewer.field = rodeo.field;
              pdfviewer.roid = rodeo.roid;
              if(resp.pages != null) pdfviewer.pages = resp.pages;
              if(resp.page != null)  pdfviewer.page  = resp.page;
              func();
              load_page_next(plane);
              viewer_plane = plane;
            });
          }
        },"json");
      }
    }
  }

  // ------------------------------
  // 隠しボタンのマウスオーバー処理
  // ------------------------------

  if(!isMobile) {
    $("#viewer_next_div_true" ).bind("vmouseover",function() { $("#viewer_next_div_true" ).css("opacity",1.0); });
    $("#viewer_next_div_true" ).bind("vmouseout", function() { $("#viewer_next_div_true" ).css("opacity",BTN_OPACITY); });
    $("#viewer_prev_div_true" ).bind("vmouseover",function() { $("#viewer_prev_div_true" ).css("opacity",1.0); });
    $("#viewer_prev_div_true" ).bind("vmouseout", function() { $("#viewer_prev_div_true" ).css("opacity",BTN_OPACITY); });
    $("#viewer_conf_div_true" ).bind("vmouseover",function() { $("#viewer_conf_div_true" ).css("opacity",1.0); });
    $("#viewer_conf_div_true" ).bind("vmouseout", function() { $("#viewer_conf_div_true" ).css("opacity",BTN_OPACITY); });
    $("#viewer_back_div_true" ).bind("vmouseover",function() { $("#viewer_back_div_true" ).css("opacity",1.0); });
    $("#viewer_back_div_true" ).bind("vmouseout", function() { $("#viewer_back_div_true" ).css("opacity",BTN_OPACITY); });

    $("#viewer_next_div_false").bind("vmouseover",function() { $("#viewer_next_div_false").css("opacity",1.0); });
    $("#viewer_next_div_false").bind("vmouseout", function() { $("#viewer_next_div_false").css("opacity",BTN_OPACITY); });
    $("#viewer_prev_div_false").bind("vmouseover",function() { $("#viewer_prev_div_false").css("opacity",1.0); });
    $("#viewer_prev_div_false").bind("vmouseout", function() { $("#viewer_prev_div_false").css("opacity",BTN_OPACITY); });
    $("#viewer_conf_div_false").bind("vmouseover",function() { $("#viewer_conf_div_false").css("opacity",1.0); });
    $("#viewer_conf_div_false").bind("vmouseout", function() { $("#viewer_conf_div_false").css("opacity",BTN_OPACITY); });
    $("#viewer_back_div_false").bind("vmouseover",function() { $("#viewer_back_div_false").css("opacity",1.0); });
    $("#viewer_back_div_false").bind("vmouseout", function() { $("#viewer_back_div_false").css("opacity",BTN_OPACITY); });
  }

  // --------------------------
  // オペレーションボタンの設定
  // --------------------------

  function button_setup(plane) {
    // クリックエリアの生成
    var opacity = BTN_OPACITY;
    var margin = 10;
    var slider = (isChrome) ? <%=SLIDER_HEIGHT%> : 0;
    var offset = 0;
    var tool = 0;
    if(!isMobile) {
      if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") offset = 17;
    }
    var                       size =  64;
    if(isIphone)              size =  96;
    if(isIpad || isAndroid)   size = 128;
    $("#viewer_next_div_"+plane).width(size); $("#viewer_next_div_"+plane).height(size);
    $("#viewer_prev_div_"+plane).width(size); $("#viewer_prev_div_"+plane).height(size);
    $("#viewer_conf_div_"+plane).width(size); $("#viewer_conf_div_"+plane).height(size);
    $("#viewer_back_div_"+plane).width(size); $("#viewer_back_div_"+plane).height(size);
    //
    $("#viewer_next_img_"+plane).width(size); $("#viewer_next_img_"+plane).height(size);
    $("#viewer_prev_img_"+plane).width(size); $("#viewer_prev_img_"+plane).height(size);
    $("#viewer_conf_img_"+plane).width(size); $("#viewer_conf_img_"+plane).height(size);
    $("#viewer_back_img_"+plane).width(size); $("#viewer_back_img_"+plane).height(size);
    //
    $("#viewer_next_div_"+plane ).css("opacity",opacity);
    $("#viewer_prev_div_"+plane ).css("opacity",opacity);
    $("#viewer_conf_div_"+plane ).css("opacity",opacity);
    $("#viewer_back_div_"+plane ).css("opacity",opacity);
    //
    $("#viewer_next_div_"+plane).css("left",margin + "px");
    $("#viewer_next_div_"+plane).css("top" ,($(window).height() - size - margin) + 'px');
    $("#viewer_prev_div_"+plane).css("left",($(window).width() - size - margin - offset) + "px");
    $("#viewer_prev_div_"+plane).css("top" ,($(window).height() - size - margin) + 'px');
    $("#viewer_conf_div_"+plane).css("left",($(window).width() - size - margin - offset) + "px");
    $("#viewer_conf_div_"+plane).css("top" ,(tool + margin) + "px");
    $("#viewer_back_div_"+plane).css("left",(margin) + "px");
    $("#viewer_back_div_"+plane).css("top" ,(tool + margin) + "px");
  }

  // --------------------------------------
  // オペレーションボタンの有効・無効の設定
  // --------------------------------------

  function setup_opbutton(plane) {
    var fenable = false;
    if(pdfviewer.mode == "yoko") {
      if(pdfviewer.page > 1) {
        fenable = true;
      }
    } else if(pdfviewer.mode == "yoko2") {
      if(pdfviewer.page > 2) {
        fenable = true;
      }
    } else {
      if(pdfviewer.mode == "tate2") {
        if(pdfviewer.page < (pdfviewer.pages - 1)) {
          fenable = true;
        }
      } else {
        if(pdfviewer.page < (pdfviewer.pages)) {
          fenable = true;
        }
      }
    }
    var benable = false;
    if(pdfviewer.mode == "yoko2") {
      if(pdfviewer.page < (pdfviewer.pages - 1)) {
        benable = true;
      }
    } else if(pdfviewer.mode == "yoko") {
      if(pdfviewer.page < (pdfviewer.pages)) {
        benable = true;
      }
    } else {
      if(pdfviewer.page > 1) {
        benable = true;
      }
    }
    if(pdfviewer.buttondir == "normal") {
      if(fenable) $("#viewer_next_div_"+plane).css("display","block");
      else        $("#viewer_next_div_"+plane).css("display","none");
      if(benable) $("#viewer_prev_div_"+plane).css("display","block");
      else        $("#viewer_prev_div_"+plane).css("display","none");
    } else {
      if(benable) $("#viewer_next_div_"+plane).css("display","block");
      else        $("#viewer_next_div_"+plane ).css("display","none");
      if(fenable) $("#viewer_prev_div_"+plane).css("display","block");
      else        $("#viewer_prev_div_"+plane).css("display","none");
    }
  }
  //
  $("#viewer_true" ).bind("pagebeforeshow",function(e) {
    // タイトルの変更
    document.title = '（' + pdfviewer.page + ' / ' + pdfviewer.pages + '）' + rodeo.title;
    button_setup(true);
    $("#viewer_content_true" ).animate({ scrollTop:0 },"fast");
    $("#viewer_content_false").animate({ scrollTop:0 },"fast");
  });
  $("#viewer_false").bind("pagebeforeshow",function(e) {
    // タイトルの変更
    document.title = '（' + pdfviewer.page + ' / ' + pdfviewer.pages + '）' + rodeo.title;
    button_setup(false);
    $("#viewer_content_true" ).animate({ scrollTop:0 },"fast");
    $("#viewer_content_false").animate({ scrollTop:0 },"fast");
  });
  // viewerページ表示
  $("#viewer_true").bind("pageshow",function(e) {
    setTimeout("scrollTo(0,1)", 100);
    $("#viewer_content_true" ).focus();
  });
  // viewerページ表示
  $("#viewer_false").bind("pageshow",function(e) {
    setTimeout("scrollTo(0,1)", 100);
    $("#viewer_content_false").focus();
  });
  // viewerページ送り
  function viewer_next(delta) {
    if(pdfviewer.mode == "tate2" || pdfviewer.mode == "yoko2") {
      delta *= 2;
    }
    if(pdfviewer.mode == "yoko" || pdfviewer.mode == "yoko2") {
      // 前ページ
      if(pdfviewer.page > delta) {
        var plane = !viewer_plane;
        load_page(plane,true,-delta,function() {
          $.mobile.changePage("#viewer_"+plane,{
            transition:"<%=PAGE_EFFECT%>",
            reverse:true
            // changeHash:false
            
          });
        });
      }
    } else {
      // 次ページ
      if(pdfviewer.page < pdfviewer.pages) {
        var plane = !viewer_plane;
        load_page(plane,false,delta,function() {
          $.mobile.changePage("#viewer_"+plane,{
            transition:"<%=PAGE_EFFECT%>",
            reverse:true
            // changeHash:false
          });
        });
      }
    }
  }
  // viewerページ戻し
  function viewer_prev(delta) {
    if(pdfviewer.mode == "tate2" || pdfviewer.mode == "yoko2") {
      delta *= 2;
    }
    if(pdfviewer.mode == "yoko" || pdfviewer.mode == "yoko2") {
      // 次ページ
      if(pdfviewer.page < pdfviewer.pages) {
        var plane = !viewer_plane;
        load_page(plane,false,delta,function() {
          $.mobile.changePage("#viewer_"+plane,{
            transition:"<%=PAGE_EFFECT%>",
            reverse:false
            // changeHash:false
          });
        });
      }
    } else {
      // 前ページ
      if(pdfviewer.page > delta) {
        var plane = !viewer_plane;
        load_page(plane,true,-delta,function() {
          $.mobile.changePage("#viewer_"+plane,{
            transition:"<%=PAGE_EFFECT%>",
            reverse:false
            // changeHash:false
          });
        });
      }
    }
  }
  // スワイプ
  $("#viewer_true,#viewer_false").bind("swiperight",function() {
    if(!loupe_display) {
      if(pdfviewer.swipe) viewer_next(1);
    }
  });
  $("#viewer_true,#viewer_false").bind("swipeleft" ,function() {
    if(!loupe_display) {
      if(pdfviewer.swipe) viewer_prev(1);
    }
  });
  // 戻る・進むボタン
  $("#viewer_prev_area_true,#viewer_prev_area_false").live("vclick",function(e) {
    // console.log("vclick");
    if(!loupe_display) {
      if(pdfviewer.buttondir == "normal") viewer_prev(1);
      else                                viewer_next(1);
    }
  });
  $("#viewer_next_area_true,#viewer_next_area_false").live("vclick",function(e) {
    // console.log("vclick");
    if(!loupe_display) {
      if(pdfviewer.buttondir == "normal") viewer_next(1);
      else                                viewer_prev(1);
    }
  });
  // 一覧に戻る
  $("#viewer_back_area_true,#viewer_back_area_false").live("vclick",function() {
    if(!loupe_display) {
      // 画像の消去
      var data = {
        "action" : "cleanup",
        "userid" : rodeo.user,
        "table"  : rodeo.table,
        "field"  : rodeo.field,
        "roid"   : rodeo.roid
      }
      $.post("<%=APP_NAME%>",data,function(resp) {});
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(record_plane,function() {
        $.mobile.changePage("#record_"+record_plane,{
          transition:"<%=VIEWER_EFFECT%>",
          reverse:!<%=VIEWER_DIRECTION%>
        });
      });
    }
  });

  // --------------------
  // Viewerリジューム処理
  // --------------------

  function viewer_resume(func) {
    viewer_next_resp = null;
    viewer_lock = false;
    var data = {
      action : "viewer_resume",
      userid : rodeo.user,
      table  : rodeo.table,
      field  : rodeo.field,
      roid   : rodeo.roid,
    }
    // POSTリクエスト
    $.mobile.loadingMessage = "ページ展開中...";
    $.mobile.showPageLoadingMsg();
    $.post("<%=APP_NAME%>",data,function(resp) {
      // タイマーを解除
      if(resp.result == true) {
        // オートリジュームの処理
        var oldpage        = pdfviewer.page;
        pdfviewer.page     = resp.page;
        pdfviewer.color    = resp.color;
        pdfviewer.mode     = resp.mode;
        pdfviewer.contrast = resp.contrast;
        pdfviewer.pages    = resp.pages;
        pdfviewer.deskew   = resp.deskew;
        pdfviewer.trim     = Number(resp.trim);
        pdfviewer.rotate   = resp.rotate;
        // 「ページ展開中...」の消去
        $.mobile.hidePageLoadingMsg();
        if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
          $("#viewer_content_true" ).css("overflow-y","scroll");
          $("#viewer_content_false").css("overflow-y","scroll");
        } else {
          $("#viewer_content_true" ).css("overflow-y","hidden");
          $("#viewer_content_false").css("overflow-y","hidden");
        }
        // ページの読み込み
        setTimeout(function() {
          $.mobile.loadingMessage = "ページ抽出中...";
          $.mobile.showPageLoadingMsg();
          func();
        },1);
      } else {
        // 「確認中...」の消去
        $.mobile.hidePageLoadingMsg();
        if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
          $("#viewer_content_true" ).css("overflow-y","scroll");
          $("#viewer_content_false").css("overflow-y","scroll");
        } else {
          $("#viewer_content_true" ).css("overflow-y","hidden");
          $("#viewer_content_false").css("overflow-y","hidden");
        }
        pdfviewer.page = (isMobile) ? 1 : 2;
        // ページの読み込み
        setTimeout(function() {
          $.mobile.loadingMessage = "ページ抽出中...";
          $.mobile.showPageLoadingMsg();
          func();
        },1);
      }
    },"json");
  }

  // ------------------
  // 一覧要素タップ処理
  // ------------------

  function record_tap(obj) {
    record_menu_exec_viewer = false;
    record_menu_exec_loader = false;
    record_menu_obj = obj;
    var a = $(record_menu_obj).attr("name").split("|",11);
    rodeo.roid  = a[0];
    rodeo.field = a[1];
    rodeo.title = a[10];
    // ダイアログ表示
    $.mobile.changePage("#record_menu",{
      transition:"pop",
      reverse:false
      // changeHash:true
    });
  }
  // ダイアログ表示
  $("#record_menu").bind("pagebeforeshow",function() {
    var a = $(record_menu_obj).attr("name").split("|",11);
    $("#record_menu_title" ).text('書名 : ' + a[10]);
    $("#record_menu_author").text('著者 : ' + a[6]);
    $("#record_menu_distr" ).text('出版 : ' + a[8]);
    if(a[2] == "") {
      $("#record_menu_isbn"  ).text('ISBN : N/A');
    } else {
      $("#record_menu_isbn"  ).text('ISBN : ' + a[2]);
    }
    $("#record_menu_status").text('状態 : ' + a[7]);
    if(a[4] == "true") {
      $("#record_menu_cover").removeClass("ui-disabled");
      $("#record_menu_download").removeClass("ui-disabled");
    } else {
      $("#record_menu_cover").addClass("ui-disabled");
      $("#record_menu_download").addClass("ui-disabled");
    }
    if(a[10].match(/\.pdf$/i)) {
      $("#record_menu_twitter").addClass("ui-disabled");
      $("#record_menu_facebook").addClass("ui-disabled");
      $("#record_menu_amazon").addClass("ui-disabled");
    } else {
      $("#record_menu_twitter").removeClass("ui-disabled");
      $("#record_menu_facebook").removeClass("ui-disabled");
      if(a[9] != "null") {
        $("#record_menu_amazon").removeClass("ui-disabled");
      } else {
        $("#record_menu_amazon").addClass("ui-disabled");
      }
    }
    if(a[6] == null || a[6] == "") {
      $("#record_menu_author_search").addClass("ui-disabled");
    } else {
      $("#record_menu_author_search").removeClass("ui-disabled");
    }
    $("#record_menu_image").attr("src",a[5]);
    $("#record_menu_image").css("width","<%=THUMBNAIL_WIDTH%>px");
    if(a[3] == "true") $("#record_menu_mark").attr("checked",true ).checkboxradio("refresh");
    else               $("#record_menu_mark").attr("checked",false).checkboxradio("refresh");
  });
  // マーク
  $("#record_menu_mark").bind("change",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    if($("#record_menu_mark").attr("checked") == "checked") {
      var newname = a[0]+'|'+a[1]+'|'+a[2]+'|true|'+a[4]+'|'+a[5]+'|'+a[6]+'|'+a[7]+'|'+a[8]+'|'+a[9]+'|'+a[10];
      $(record_menu_obj).attr("name",newname);
      var color = "orange";
      // if(a[4] != "true") color = "red";
      $("img",record_menu_obj).css("border-color",color);
      $("img",record_menu_obj).css("box-shadow",'0 0 8px ' + color);
      $("img",record_menu_obj).css("-webkit-box-shadow",'0 0 8px ' + color);
      $("img",record_menu_obj).css("-moz-box-shadow",'0 0 8px ' + color);
      detail_mark(true);
    } else {
      var newname = a[0]+'|'+a[1]+'|'+a[2]+'|false|'+a[4]+'|'+a[5]+'|'+a[6]+'|'+a[7]+'|'+a[8]+'|'+a[9]+'|'+a[10];
      $(record_menu_obj).attr("name",newname);
      var color = "gray";
      if(a[4] != "true") color = "red";
      $("img",record_menu_obj).css("border-color",color);
      $("img",record_menu_obj).css("box-shadow",'0 0 8px ' + color);
      $("img",record_menu_obj).css("-webkit-box-shadow",'0 0 8px ' + color);
      $("img",record_menu_obj).css("-moz-box-shadow",'0 0 8px ' + color);
      detail_mark(false);
    }
  });
  // 読む
  $("#record_menu_cover").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    if(a[4] == "true") {
      var title = a[10];
      pdfviewer.title = title;
      if($(window).width() < $(window).height()) {
        if(isMobile) {
          pdfviewer.mode = "comic";
        } else {
          pdfviewer.mode = "tate";
        }
      } else {
        pdfviewer.mode = "tate2";
      }
      pdfviewer.page     = 2;
      pdfviewer.contrast = "1";
      pdfviewer.deskew   = true;
      pdfviewer.trim     = 0;
      cursorTimer        = null;
      store_state();
      viewer_resume(function() {
        // ページの読み込み
        load_page(viewer_plane,false,0,function() {
          $.mobile.changePage("#viewer_"+viewer_plane,{
            transition:"<%=VIEWER_EFFECT%>",
            reverse:<%=VIEWER_DIRECTION%>
            // changeHash:true
          });
        });
      });
    }
  });
  // シリーズを検索
  $("#record_menu_search").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    name = encodeURI(a[10].split(/[ (<（＜0123456789０１２３４５６７８９]/)[0]);
    location.href = "<%=APP_NAME%>?slave=yes&where="+name;
  });
  // 著者で検索
  $("#record_menu_author_search").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    name = encodeURI(a[6].replace(/\s+/g,""));
    location.href = "<%=APP_NAME%>?slave=yes&where="+name;
  });
  // ダウンロードして読む
  $("#record_menu_download").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    if(a[4] == "true") {
      href = '<%=APP_NAME%>?action=pdfdownload&table='+encodeURI(rodeo["table"])+'&field='+encodeURI(rodeo["field"])+'&roid='+a[0];
      // アプリケーションショーツカットから起動しているとき
      if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
        window.open(href,"download","menubar=no,toolbar=no,location=no,status=no");
      } else {
        window.open(href,"download");
      }
      $.mobile.changePage("#record_"+record_plane,{
        transition:"pop",
        reverse:true
        // changeHash:true
      });
    }
  });
  // ツイートする
  $("#record_menu_twitter").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    if(a[4] == "true") {
      if(a[2] != "null") {
        var url  = 'http://'+location.host+'/<%=APP_NAME%>?action=facebook&user='+rodeo.user+'&roid='+a[0];
        var from = '+<%=TWITTER_ID%> より';
        var href = 'https://twitter.com/intent/tweet?status='+encodeURIComponent('【'+a[10]+'】\\n'+url+from);
      } else {
        var href = 'https://twitter.com/intent/tweet?status='+encodeURIComponent('"'+a[10]+'"');
      }
      // アプリケーションショーツカットから起動しているとき
      if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
        window.open(href,"Twitter","menubar=no,toolbar=no,location=no,status=no");
      } else {
        window.open(href,"Twitter");
      }
      $.mobile.changePage("#record_"+record_plane,{
        transition:"pop",
        reverse:true
        // changeHash:true
      });
    }
  });
  // フェイスブック
  $("#record_menu_facebook").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    if(a[4] == "true") {
      var user = rodeo.user;
      var url  = 'http://'+location.host+'/<%=APP_NAME%>?action=facebook&user='+user+'&roid='+a[0];
      var href = 'http://www.facebook.com/share.php?t='+encodeURIComponent(a[10])+'&u='+encodeURIComponent(url);
      // アプリケーションショーツカットから起動しているとき
      if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
        window.open(href,"Facebook","menubar=no,toolbar=no,location=no,status=no");
      } else {
        window.open(href,"Facebook");
      }
      $.mobile.changePage("#record_"+record_plane,{
        transition:"pop",
        reverse:true
        // changeHash:true
      });
    }
  });
  // アマゾンで詳細を見る
  $("#record_menu_amazon").bind("vclick",function(e,ui) {
    var a = $(record_menu_obj).attr("name").split("|",11);
    if(a[4] == "true") {
      if(a[9] != "null") {
        if(isIphone || isAndroid) {
          var href = 'http://www.amazon.co.jp/exec/obidos/ASIN/'+a[9]+'/<%=AMAZON_ASSOCID%>/ref=nosim/';
          location.href = href;
        } else {
          var href = 'http://www.amazon.co.jp/exec/obidos/ASIN/'+a[9]+'/<%=AMAZON_ASSOCID%>/ref=nosim/';
          // アプリケーションショーツカットから起動しているとき
          if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
            window.open(href,"amazon","menubar=no,toolbar=no,location=no,status=no");
          } else {
            window.open(href,"amazon");
          }
        }
        $.mobile.changePage("#record_"+record_plane,{
          transition:"pop",
          reverse:true
          // changeHash:true
        });
      }
    }
  });
  // 閉じる
  $("#record_menu_exec").bind("vclick",function() {
    $.mobile.changePage("#record_"+record_plane,{
      transition:"pop",
      reverse:true
      // changeHash:true
    });
  });

  // --------------------
  // レコード一覧読み込み
  // --------------------

  function load_recordlist(plane,func) {
    // 表示パラメータの計算
    var opacity = "1.0";
    if(isIphone || isAndroid || isKindle) {
      var sw = <%=THUMBNAIL_WIDTH%>  * <%=THUMBNAIL_MOBILE%>;
      var sh = <%=THUMBNAIL_HEIGHT%> * <%=THUMBNAIL_MOBILE%>;
    } else {
      var sw = <%=THUMBNAIL_WIDTH%>  * <%=THUMBNAIL_PC%>;
      var sh = <%=THUMBNAIL_HEIGHT%> * <%=THUMBNAIL_PC%>;
    }
    var border = 44;
    var color = "gray";
    // 横表示数の計算
    var nx = Math.floor($(window).width() / (sw + 40));
    if(nx > 7) nx = 7;
    var xr = (100 / nx);
    // 縦表示数の計算
    var cy = $(window).height() - <%=TITLEBAR_SIZE%> - 2; // ヘッダ分を引く
    cy -= 15;
    var ny = Math.floor(cy / (sh + border));
    rodeo.hgrid = nx;
    rodeo.listlen = nx * ny;
    var left = cy - (ny * (sh + border));
    var pad = left / ny / 2;
    var yr = (100 / ny);
    // POSTデータ作成
    var data = {
      action : "record",
      userid : rodeo.user,
      table  : rodeo.table,
      offset : rodeo.offset,
      where  : rodeo.where,
      mark   : rodeo.mark,
      order  : rodeo.order,
      dir    : rodeo.dir,
      count  : rodeo.listlen
    }
    // POSTリクエスト
    $.post("<%=APP_NAME%>",data,function(resp) {
      if(resp.result == true) {
        record_plane = plane;
        rodeo.count = resp.count;
        rodeo.roid = null;
        //
        store_state();
        // テーブル名の設定
        $("#record_table_name_"+plane).empty();
        $("#record_table_name_"+plane).append(''+(rodeo.offset+1)+' - '+(rodeo.offset+rodeo.listlen)+' / '+resp.count);
        //
        var grids;
        switch(nx) {
          case 0:
          case 1:
          case 2: grids = 'ui-grid-a'; break;
          case 3: grids = 'ui-grid-b'; break;
          case 4: grids = 'ui-grid-c'; break;
          case 5: grids = 'ui-grid-d'; break;
          case 6: grids = 'ui-grid-e'; break;
          case 7: grids = 'ui-grid-f'; break;
        }
        //
        var ul = '<div id="record_list" class="' + grids + '">';
        var title_style = 'font-size: 70%; margin-top: -8px; margin-left: 2px; margin-right: 2px; padding: 2px;' +
            ' font-weight: bold; color: #555; text-shadow: 1px 1px 1px #BBB;' +
            ' text-overflow: ellipsis; -webkit-text-overflow: ellipsis; -o-text-overflow: ellipsis;' +
            ' white-space: nowrap; overflow: hidden;';
        var prefetct_count = 0;
        var callback_done = false;
        for(var i = 0; i < rodeo.listlen; i += rodeo.hgrid) {
          for(var j = 0; j < rodeo.hgrid; j ++) {
            if((i+j) < resp.records.length) {
              color = "gray";
              opacity = "1.0";
              if(resp.records[i+j]["mark"] == true) {
                color = "orange";
              }
              if(resp.records[i+j]["enable"] != true) {
                color = "red";
                opacity = "0.2";
              }
            }
            var block_style = 'style="border: solid 4px ' + color + ';' +
                ' margin-top: 4px; margin-left: 4px; margin-right: 4px; margin-bottom: 8px;' +
                ' width: ' + sw + 'px; height: ' + (sh) + 'px; opacity:' + opacity + ';' +
                ' box-shadow: 0px 0px 8px ' + color + ';' +
                ' -webkit-box-shadow: 0px 0px 8px ' + color + ';' +
                ' -moz-box-shadow: 0px 0px 8px ' + color + ';' +
                '' + '"';
            var block;
            switch(j) {
              case 0: block = 'ui-block-a'; break;
              case 1: block = 'ui-block-b'; break;
              case 2: block = 'ui-block-c'; break;
              case 3: block = 'ui-block-d'; break;
              case 4: block = 'ui-block-e'; break;
              case 5: block = 'ui-block-f'; break;
              case 6: block = 'ui-block-g'; break;
            }
            var li = '<div class="' + block +
              '" style="width: ' + xr + '%; height: ' + (sh + border + pad) + 'px;' +
              ' padding-top: ' + (pad) + 'px; text-align: center;">';
            if((i+j) < resp.records.length) {
              if(resp.records[i+j]["page"] == "未読") {
                var red_text = '<span style="background-color: lightgreen; color: darkglay; padding-left: 2px; padding-right: 2px; ' +
                    ' border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;' +
                    // ' border: solid; border-width: 1px; border-color: gray;">' + resp.records[i+j]["page"] +
                    ' border: solid; border-width: 1px; border-color: gray;">U' +
                    '</span> ';
              } else {
                if(resp.records[i+j]["page"] == "読了") {
                  var red_text = '<span style="background-color: gainsboro; color: darkglay; padding-left: 2px; padding-right: 2px; ' +
                      ' border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;' +
                      // ' border: solid; border-width: 1px; border-color: gray;">' + resp.records[i+j]["page"] +
                      ' border: solid; border-width: 1px; border-color: gray;">E' +
                      '</span> ';
                } else {
                  var red_text = '<span style="background-color: orange; color: darkglay; padding-left: 2px; padding-right: 2px; ' +
                      ' border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;' +
                      // ' border: solid; border-width: 1px; border-color: gray;">' + resp.records[i+j]["page"] +
                      ' border: solid; border-width: 1px; border-color: gray;">' + resp.records[i+j]["page"].split("/")[0] +
                      '</span> ';
                }
              }
              var link_style = 'class="rodeo-no-link"';
              var link_name = 'name="' +
                        resp.records[i+j]["roid"] +
                  '|' + resp.field +
                  '|' + resp.records[i+j]["isbn"] +
                  '|' + resp.records[i+j]["mark"] +
                  '|' + resp.records[i+j]["enable"] +
                  '|' + resp.records[i+j]["cover"] +
                  '|' + resp.records[i+j]["author"] +
                  '|' + resp.records[i+j]["page"] +
                  '|' + resp.records[i+j]["distr"] +
                  '|' + resp.records[i+j]["asin"] +
                  '|' + resp.records[i+j]["subtitle"] +
                  '"';
              var hover_title = 'title="' + 
                  "題名：" + resp.records[i+j]["subtitle"] + '\\n' + 
                  "著者：" + resp.records[i+j]["author"] + '\\n' + 
                  "出版：" + resp.records[i+j]["distr"] + '\\n' + 
                  "ISBN：" + resp.records[i+j]["isbn"] + '\\n' + 
                  "状態：" + resp.records[i+j]["page"] + '\\n' +
                  "ＩＤ：" + resp.records[i+j]["roid"] + '"';
              li += '<a href="#" ' + link_style + ' ' + link_name + '>';
              li += '<img class="hastip" src="' + resp.records[i+j]["cover"] + '" ' + block_style + ' ' + hover_title + '></a>';
              li += '<div style="' + title_style + '">' + red_text + resp.records[i+j]["subtitle"] + '</div>';
              // サムネイルの先読み
              $("<img>").attr("src",resp.records[i+j]["cover"]).one("load",function() {
                prefetct_count ++;
                if(callback_done == false && prefetct_count == resp.records.length) {
                  // 「読み込み中...」の消去
                  $.mobile.hidePageLoadingMsg();
                  callback_done = true;
                  func();
                }
              });
            } else {
              var link_style = 'class="rodeo-no-link"';
              var link_name = 'name="skip"';
              li += '<a href="#" ' + link_style + ' ' + link_name + '>';
              li += '</a>';
            }
            li += '</div>';
            ul += li;
          }
        }
        ul += '</div>';
        $("#record_content_"+plane).empty();
        $("#record_content_"+plane).append(ul);
        // $('.hastip').tooltipsy();
        // li要素をtapした時のハンドラ
        $("#record_content_"+plane+" > div > div > a").tap(function() {
          if($(this).attr("name") != "skip") {
            var a = $(this).attr("name").split("|",10);
            rodeo.roid  = a[0];
            rodeo.field = a[1];
            rodeo.title = a[9];
            record_tap(this);
          }
        });
        if(resp.records.length == 0) {
          // 「読み込み中...」の消去
          $.mobile.hidePageLoadingMsg();
          callback_done = true;
          func();
        } else {
          // コールバック
          setTimeout(function() {
            if(callback_done == false) {
              $.mobile.hidePageLoadingMsg();
              callback_done = true;
              func();
            }
          },20000);
        }
      }
    });
  };

  // --------
  // 一覧表示
  // --------

  $("#record_true,#record_false").bind("pagebeforeshow",function(e,ui) {
    document.title = "<%=TITLE%>";
  });
  $("#record_true,#record_false").bind("pageshow",function(e,ui) {
    setTimeout("scrollTo(0,1)", 100);
  });

  // ----------------
  // 一覧前ページ遷移
  // ----------------

  $("#record_prev_btn_true" ).bind("vclick",function(e) {
    if(rodeo.offset >= rodeo.listlen) {
      rodeo.offset -= rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(false,function() {
        $.mobile.changePage("#record_false",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:true
          // changeHash:false
        });
      });
    }
  });
  $("#record_prev_btn_false").bind("vclick",function(e) {
    if(rodeo.offset >= rodeo.listlen) {
      rodeo.offset -= rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(true,function() {
        $.mobile.changePage("#record_true",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:true
          // changeHash:false
        });
      });
    }
  });

  // ----------------
  // 一覧次ページ遷移
  // ----------------

  $("#record_next_btn_true" ).bind("vclick",function(e) {
    if((rodeo.offset + rodeo.listlen) < rodeo.count) {
      rodeo.offset += rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(false,function() {
        $.mobile.changePage("#record_false",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:false
          // changeHash:false
        });
      });
    }
  });
  $("#record_next_btn_false").bind("vclick",function(e) {
    if((rodeo.offset + rodeo.listlen) < rodeo.count) {
      rodeo.offset += rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(true,function() {
        $.mobile.changePage("#record_true",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:false
          // changeHash:false
        });
      });
    }
  });

  // ------------
  // 一覧スワイプ
  // ------------

  $("#record_true").bind("swipeleft",function() {
    if((rodeo.offset + rodeo.listlen) < rodeo.count) {
      rodeo.offset += rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(false,function() {
        $.mobile.changePage("#record_false",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:false
          // changeHash:false
        });
      });
    }
  });
  $("#record_false").bind("swipeleft",function() {
    if((rodeo.offset + rodeo.listlen) < rodeo.count) {
      rodeo.offset += rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(true,function() {
        $.mobile.changePage("#record_true",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:false
          // changeHash:false
        });
      });
    }
  });
  
  $("#record_true").bind("swiperight",function()  {
    if(rodeo.offset >= rodeo.listlen) {
      rodeo.offset -= rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(false,function() {
        $.mobile.changePage("#record_false",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:true
          // changeHash:false
        });
      });
    }
  });
  $("#record_false").bind("swiperight",function()  {
    if(rodeo.offset >= rodeo.listlen) {
      rodeo.offset -= rodeo.listlen;
      $.mobile.loadingMessage = "読み込み中...";
      $.mobile.showPageLoadingMsg();
      load_recordlist(true,function() {
        $.mobile.changePage("#record_true",{
          transition:"<%=INDEX_EFFECT%>",
          reverse:true
          // changeHash:false
        });
      });
    }
  });

  // --------
  // 一覧更新
  // --------

  $("#record_refresh_btn_true,#record_refresh_btn_false").bind("vclick",function(e) {
    $.mobile.loadingMessage = "更新中...";
    $.mobile.showPageLoadingMsg();
    load_recordlist(false,function() {
      $.mobile.changePage("#record_false",{
        transition:"fade",
        reverse:false
        // changeHash:false
      });
    });
  });

  // ----------------------------------
  // ウインドウがリサイズされた時の処理
  // ----------------------------------

  function window_resized() {
    if($.mobile.activePage.jqmData('url') == "record_true"
    || $.mobile.activePage.jqmData('url') == "record_false") {
      // 表示パラメータの計算
      if(isIphone) {
        var sw = <%=THUMBNAIL_WIDTH%>  * <%=THUMBNAIL_MOBILE%>;
        var sh = <%=THUMBNAIL_HEIGHT%> * <%=THUMBNAIL_MOBILE%>;
      } else {
        var sw = <%=THUMBNAIL_WIDTH%>  * <%=THUMBNAIL_PC%>;
        var sh = <%=THUMBNAIL_HEIGHT%> * <%=THUMBNAIL_PC%>;
      }
      var border = 24 + 6 + 4 + 4;
      // 横表示数の計算
      var nx = Math.floor($(window).width() / (sw + 20));
      if(nx > 7) nx = 7;
      var xr = (100 / nx);
      // 縦表示数の計算
      var cy = $(window).height() - 41;
      var ny = Math.floor(cy / (sh + border));
      // 表示数に変化がある場合
      if(rodeo.hgrid != nx && rodeo.listlen != (nx * ny)) {
        $.mobile.loadingMessage = "読み込み中...";
        $.mobile.showPageLoadingMsg();
        // 再表示
        var plane = !record_plane;
        load_recordlist(plane,function() {
          $.mobile.changePage("#record_"+(plane),{
            transition:"fade",
            reverse:false
            // changeHash:false
          });
        });
      }
    } else if($.mobile.activePage.jqmData('url') == "viewer_true"
           || $.mobile.activePage.jqmData('url') == "viewer_false") {
      viewer_resume(function() {
        // ページの読み込み
        load_page(!viewer_plane,false,0,function() {
          $.mobile.changePage("#viewer_"+(!viewer_plane),{
            transition:"fade",
            reverse:false
            // changeHash:false
          });
        });
      });
    }
  }
  // 端末の向きの変更
  $(window).bind("orientationchange", function(e) {
    if(isIphone || isIpad) {
      setTimeout(function() {
        viewer_setup();
        window_resized();
      },1);
    }
  });
  // リサイズ
  $(window).bind("resize",function(e) {
    if(viewer_resize_timer) {
      clearTimeout(viewer_resize_timer);
      viewer_resize_timer = null;
    }
    // 100msイベントが発生しない
    viewer_resize_timer = setTimeout(function() {
      viewer_setup();
      // window_resized();
    },100);
  });

  // ------------------------
  // 検索条件ダイアログの処理
  // ------------------------

  // 初期値の設定
  $("#find_dialog").bind("pagebeforeshow",function(e,ui) {
    // マーク
    $("#order_mark1").attr("checked",false).checkboxradio("refresh");
    $("#order_mark2").attr("checked",false).checkboxradio("refresh");
    if     (rodeo.mark == "all") $("#order_mark1").attr("checked",true).checkboxradio("refresh");
    else if(rodeo.mark == "on")  $("#order_mark2").attr("checked",true).checkboxradio("refresh");
    else alert("invalid mark: " + rodeo.mark);
    // 並び替え項目
    $("#order_field1").attr("checked",false).checkboxradio("refresh");
    $("#order_field2").attr("checked",false).checkboxradio("refresh");
    if     (rodeo.order == "roid") $("#order_field1").attr("checked",true).checkboxradio("refresh");
    else if(rodeo.order == "name") $("#order_field2").attr("checked",true).checkboxradio("refresh");
    else alert("invalid order: " + rodeo.order);
    // 並び替え順序
    $("#order_dir1").attr("checked",false).checkboxradio("refresh");
    $("#order_dir2").attr("checked",false).checkboxradio("refresh");
    if     (rodeo.dir == "asc" ) $("#order_dir1").attr("checked",true).checkboxradio("refresh");
    else if(rodeo.dir == "desc") $("#order_dir2").attr("checked",true).checkboxradio("refresh");
    else alert("invalid dir: " + rodeo.dir);
    // 検索文字列
    $("#find_dialog_qstring").val(rodeo.where);
    // 検索履歴
    $("#search_history").empty();
    $("#search_history").append('<option value="">--- 選択してください ---</option>');
    for(var i = 0; i < searchHistory.length; i ++) {
      var str = '<option value="' + searchHistory[i] + '">' + searchHistory[i] + '</option>';
      $("#search_history").append('<option value="' + searchHistory[i] + '">' + searchHistory[i] + '</option>').trigger("create");
    }
    $("#search_history")[0].selectIndex = 0;
    $("#search_history").selectmenu("refresh");
  });
  // 検索履歴が選ばれた時の処理
  $("#search_history").change(function() {
    str = $("#search_history option:selected").val();
    $("#find_dialog_qstring").val(str);
  });
  // 実行ボタンの処理
  $("#find_dialog_exec").bind("vclick",function(e) {
    rodeo.mark  = $('[name=order_mark]:checked').val();
    rodeo.order = $('[name=order_field]:checked').val();
    rodeo.dir   = $('[name=order_dir]:checked').val();
    rodeo.where = jQuery.trim($("#find_dialog_qstring").val());
    if(rodeo.where != "") {
      var find = -1;
      for(var i = 0; i < searchHistory.length; i ++) {
        if(searchHistory[i] == rodeo.where) {
          find = i;
        }
      }
      // 見つからないとき
      if(find == -1) {
        // 配列に保存
        searchHistory.unshift(rodeo.where);
        // 20個以上の場合は削除
        while(searchHistory.length > rodeo.history_size) {
          searchHistory.pop();
        }
      } else {
        searchHistory.splice(find,1);
        searchHistory.unshift(rodeo.where);
      }
      store_history();
    }
    store_state();
    rodeo.offset = 0;
    $.mobile.loadingMessage = "読み込み中...";
    $.mobile.showPageLoadingMsg();
    var plane = !record_plane;
    load_recordlist(plane,function() {
      $.mobile.changePage("#record_"+plane,{
        transition:"pop",
        reverse:true
        // changeHash:true
      });
    });
  });

  // 閉じる
  $("#find_dialog").bind("pagehide",function(e) {
    $.mobile.changePage("#record_"+record_plane,{
      transition:"pop",
      reverse:true
      // changeHash:true
    });
  });

  // --------------------
  // viewer設定ダイアログ
  // --------------------

  $("#viewer_conf_area_true,#viewer_conf_area_false").bind("vclick",function() {
    $.mobile.changePage("#viewer_dialog",{
      transition:"pop",
      reverse:false
      // changeHash:true
    });
  });
  $("#viewer_dialog").bind("pagebeforeshow",function(e) {
    // カラー
    $("#color-mode1").attr("checked",false).checkboxradio("refresh");
    $("#color-mode2").attr("checked",false).checkboxradio("refresh");
    $("#color-mode3").attr("checked",false).checkboxradio("refresh");
    if     (pdfviewer.color == "color") $("#color-mode1").attr("checked", true).checkboxradio("refresh");
    else if(pdfviewer.color == "gray" ) $("#color-mode2").attr("checked", true).checkboxradio("refresh");
    else if(pdfviewer.color == "mono" ) $("#color-mode3").attr("checked", true).checkboxradio("refresh");
    else $("#color-mode1").attr("checked", true).checkboxradio("refresh");
    // コントラスト
    $("#contrast-mode0").attr("checked",false).checkboxradio("refresh");
    $("#contrast-mode1").attr("checked",false).checkboxradio("refresh");
    $("#contrast-mode2").attr("checked",false).checkboxradio("refresh");
    $("#contrast-mode3").attr("checked",false).checkboxradio("refresh");
    $("#contrast-mode4").attr("checked",false).checkboxradio("refresh");
    $("#contrast-mode5").attr("checked",false).checkboxradio("refresh");
    if     (pdfviewer.contrast == "0") $("#contrast-mode0").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.contrast == "1") $("#contrast-mode1").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.contrast == "2") $("#contrast-mode2").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.contrast == "3") $("#contrast-mode3").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.contrast == "4") $("#contrast-mode4").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.contrast == "5") $("#contrast-mode4").attr("checked",true).checkboxradio("refresh");
    else $("#contrast-mode0").attr("checked",true).checkboxradio("refresh");
    $("#contrast-mode0").checkboxradio("enable");
    $("#contrast-mode1").checkboxradio("enable");
    $("#contrast-mode2").checkboxradio("enable");
    $("#contrast-mode3").checkboxradio("enable");
    $("#contrast-mode4").checkboxradio("enable");
    $("#contrast-mode5").checkboxradio("enable");
    // 版型
    $("#page-mode1").attr("checked",false).checkboxradio("refresh");
    $("#page-mode2").attr("checked",false).checkboxradio("refresh");
    $("#page-mode3").attr("checked",false).checkboxradio("refresh");
    $("#page-mode4").attr("checked",false).checkboxradio("refresh");
    $("#page-mode5").attr("checked",false).checkboxradio("refresh");
    if     (pdfviewer.mode == "comic") $("#page-mode1").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.mode == "tate")  $("#page-mode2").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.mode == "tate2") $("#page-mode3").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.mode == "yoko")  $("#page-mode4").attr("checked",true).checkboxradio("refresh");
    else if(pdfviewer.mode == "yoko2") $("#page-mode5").attr("checked",true).checkboxradio("refresh");
    else alert("invalid page mode: " + pdfviewer.mode);
    // 傾斜補正
    if(pdfviewer.deskew == true) $("#deskew").attr("checked", true).checkboxradio("refresh");
    else                         $("#deskew").attr("checked",false).checkboxradio("refresh");
    // 回転補正
    if(pdfviewer.rotate == true) $("#rotate").attr("checked", true).checkboxradio("refresh");
    else                         $("#rotate").attr("checked",false).checkboxradio("refresh");
    // スワイプ
    if(pdfviewer.swipe == true) $("#swipe").attr("checked", true).checkboxradio("refresh");
    else                        $("#swipe").attr("checked",false).checkboxradio("refresh");
    // 余白削除
    $("#trim").val(pdfviewer.trim).slider("refresh");
    // ボタンの向き
    if(pdfviewer.buttondir == "normal") $("#buttondir-mode").attr("checked",false).checkboxradio("refresh");
    else                                $("#buttondir-mode").attr("checked",true ).checkboxradio("refresh");
    // ページスライダ
    $("#viewer_slider").attr("max",pdfviewer.pages);
    $("#viewer_slider").val(pdfviewer.page).slider("refresh");
  });
  // ダイアログ実行
  $("#viewer_dialog_exec").bind("vclick",function(ee) {
    pdfviewer.color     = $('[name=color-mode]:checked'   ).val();
    pdfviewer.contrast  = $('[name=contrast-mode]:checked').val();
    pdfviewer.mode      = $('[name=page-mode]:checked'    ).val();
    pdfviewer.deskew    = ($('[name=deskew]:checked').length != 0) ? true : false;
    pdfviewer.rotate    = ($('[name=rotate]:checked').length != 0) ? true : false;
    pdfviewer.swipe     = ($('[name=swipe]:checked').length != 0) ? true : false;
    pdfviewer.buttondir = ($('[name=buttondir-mode]:checked').length != 0) ? "reverse" : "normal";
    pdfviewer.trim      = eval($("#trim").val());
    if(pdfviewer.mode == "comic" || pdfviewer.mode == "yoko") {
      $("#viewer_content_true" ).css("overflow-y","scroll");
      $("#viewer_content_false").css("overflow-y","scroll");
      $("#viewer_content_true" ).css("text-align","left");
      $("#viewer_content_false").css("text-align","left");
    } else {
      $("#viewer_content_true" ).css("overflow-y","hidden");
      $("#viewer_content_false").css("overflow-y","hidden");
      $("#viewer_content_true" ).css("text-align","center");
      $("#viewer_content_false").css("text-align","center");
    }
    var offset = Number($("#viewer_slider").val());
    if(offset != pdfviewer.page) {
      pdfviewer.page = offset;
    }
    // 先読みの無効化
    viewer_next_resp = null;
    // ページの再読み込み
    var plane = !viewer_plane;
    load_page(plane,true,0,function() {
      $.mobile.changePage("#viewer_"+plane,{
        transition:"pop",
        reverse:true
        // changeHash:false
      });
    });
  });
  // 閉じる
  $("#viewer_dialog").bind("pagehide",function(e) {
    $.mobile.changePage("#viewer_"+viewer_plane,{
      transition:"pop",
      reverse:true
      // changeHash:true
    });
  });

  // ------------
  // ルーペの表示
  // ------------

/*
  var mouseup_count = 0;
  $(window).bind("vmousedown",function(e) {
    if(!isMobile) {
      // if(!viewer_lock) {
        // console.log("vmousedown");
        if($.mobile.activePage.jqmData('url') == "viewer_true"
        || $.mobile.activePage.jqmData('url') == "viewer_false" ) {
          var old_count = mouseup_count;
          var viewerTimer = setTimeout(function() {
            if(mouseup_count == old_count) {
              loupe_display = true;
              var offsetX = ($(window).width() - $("#viewer_img_"+viewer_plane).width()) / 2;
              var offsetY = -$("#viewer_content_"+viewer_plane).scrollTop();
              //
              var px = e.pageX - (pdfviewer.loupe_size / 2);
              var py = e.pageY - (pdfviewer.loupe_size / 2);
              $("#viewer_loupe_"+viewer_plane).css("left",px);
              $("#viewer_loupe_"+viewer_plane).css("top", py);
              //
              var dx = (pdfviewer.loupe_size / 2) - ((e.pageX - offsetX) * pdfviewer.magnify) - 2;
              var dy = (pdfviewer.loupe_size / 2) - ((e.pageY - offsetY) * pdfviewer.magnify) - 2;
              $("#viewer_loupe_"+viewer_plane).css("background-position",dx + 'px ' + dy + 'px');
              $("#viewer_loupe_"+viewer_plane).fadeIn("fast");
            }
          },500);
        }
      // }
    }
  });
  $(window).bind("vmousemove",function(e) {
    if(!isMobile) {
      // if(!viewer_lock) {
        if($.mobile.activePage.jqmData('url') == "viewer_true"
        || $.mobile.activePage.jqmData('url') == "viewer_false" ) {
          if(loupe_display == true) {
            //
            var offsetX = ($(window).width() - $("#viewer_img_"+viewer_plane).width()) / 2;
            var offsetY = -$("#viewer_content_"+viewer_plane).scrollTop();
            //
            var px = e.pageX - (pdfviewer.loupe_size / 2);
            var py = e.pageY - (pdfviewer.loupe_size / 2);
            $("#viewer_loupe_"+viewer_plane).css("left",px);
            $("#viewer_loupe_"+viewer_plane).css("top", py);
            //
            var dx = (pdfviewer.loupe_size / 2) - ((e.pageX - offsetX) * pdfviewer.magnify) - 2;
            var dy = (pdfviewer.loupe_size / 2) - ((e.pageY - offsetY) * pdfviewer.magnify) - 2;
            $("#viewer_loupe_"+viewer_plane).css("background-position",dx + 'px ' + dy + 'px');
          }
        }
      // }
    }
  });
  $(window).bind("vmouseup",function(e) {
    if(!isMobile) {
      // if(!viewer_lock) {
        // console.log("vmouseup");
        if($.mobile.activePage.jqmData('url') == "viewer_true"
        || $.mobile.activePage.jqmData('url') == "viewer_false" ) {
          if(loupe_display == true) {
            loupe_display = false;
            $("#viewer_loupe_"+viewer_plane).fadeOut("fast");
          }
        }
      // }
      mouseup_count ++;
    }
  });
*/

  // ----
  // 別窓
  // ----

  $("#new_window_true,#new_window_false").bind("vclick",function(e) {
    // アプリケーションショーツカットから起動しているとき
    /*
    if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
      window.open("<%=APP_NAME%>?slave=yes","<%=TITLE%>","menubar=no,toolbar=no,location=no,status=no");
    } else {
      window.open("<%=APP_NAME%>?slave=yes","<%=TITLE%>");
    }
    */
    location.href = "<%=APP_NAME%>?slave=yes";
  });
  $("#new_twitter_true,#new_twitter_false").bind("vclick",function(e) {
    // アプリケーションショーツカットから起動しているとき
    if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
      window.open("https://twitter.com/","Twitter","menubar=no,toolbar=no,location=no,status=no");
    } else {
      window.open("https://twitter.com/","Twitter");
    }
  });
  $("#new_facebook_true,#new_facebook_false").bind("vclick",function(e) {
    // アプリケーションショーツカットから起動しているとき
    if(isChrome && (window.outerHeight - window.innerHeight) < 40) {
      window.open("http://www.facebook.com/","facebook","menubar=no,toolbar=no,location=no,status=no");
    } else {
      window.open("http://www.facebook.com/","facebook");
    }
  });
  $("#close_button_true,#close_button_false").bind("vclick",function(e) {
    /*
    if(isIphone) {
      $.mobile.loadingMessage = "読込中...";
      $.mobile.showPageLoadingMsg();
      location.href = "<%=APP_NAME%>?slave=";
    } else {
      window.close();
    }
    */
    $.mobile.loadingMessage = "読込中...";
    $.mobile.showPageLoadingMsg();
    location.href = "<%=APP_NAME%>?slave=&return=yes";
  });

  $("#app_info").bind("pagebeforeshow",function() {
    $(body).css("overflow-y","scroll");
  });
  $("#app_info").bind("pagebeforehide",function() {
    $(body).css("overflow-y","hidden");
  });

  // ------------------
  // キーボードハンドラ
  // ------------------

  $(window).bind("keypress",function(e) {
    // ---------
    // login処理
    // ---------
    if($.mobile.activePage.jqmData('url') == "login") {
      if(e.which == 13) {
        $("#login_button").click();
      }
    // --------------------
    // 一覧ページ送り(true)
    // --------------------
    } else if($.mobile.activePage.jqmData('url') == "record_true") {
      if(e.which == 32) {
        if(e.shiftKey == true || e.ctrlKey == true) {
          if(rodeo.offset >= rodeo.listlen) {
            rodeo.offset -= rodeo.listlen;
            $.mobile.loadingMessage = "読み込み中...";
            $.mobile.showPageLoadingMsg();
            load_recordlist(false,function() {
              $.mobile.changePage("#record_false",{
                transition:"<%=INDEX_EFFECT%>",
                reverse:true
                // changeHash:false
              });
            });
          }
        } else {
          if((rodeo.offset + rodeo.listlen) < rodeo.count) {
            rodeo.offset += rodeo.listlen;
            $.mobile.loadingMessage = "読み込み中...";
            $.mobile.showPageLoadingMsg();
            load_recordlist(false,function() {
              $.mobile.changePage("#record_false",{
                transition:"<%=INDEX_EFFECT%>",
                reverse:false
                // changeHash:false
              });
            });
          }
        }
      }
    // ---------------------
    // 一覧ページ送り(false)
    // ---------------------
    } else if($.mobile.activePage.jqmData('url') == "record_false") {
      if(e.which == 32) {
        if(e.shiftKey == true || e.ctrlKey == true) {
          if(rodeo.offset >= rodeo.listlen) {
            rodeo.offset -= rodeo.listlen;
            $.mobile.loadingMessage = "読み込み中...";
            $.mobile.showPageLoadingMsg();
            load_recordlist(true,function() {
              $.mobile.changePage("#record_true",{
                transition:"<%=INDEX_EFFECT%>",
                reverse:true
                // changeHash:false
              });
            });
          }
        } else {
          if((rodeo.offset + rodeo.listlen) < rodeo.count) {
            rodeo.offset += rodeo.listlen;
            $.mobile.loadingMessage = "読み込み中...";
            $.mobile.showPageLoadingMsg();
            load_recordlist(true,function() {
              $.mobile.changePage("#record_true",{
                transition:"<%=INDEX_EFFECT%>",
                reverse:false
                // changeHash:false
              });
            });
          }
        }
      }
    // ------------------
    // viewerのページ送り
    // ------------------
    } else if($.mobile.activePage.jqmData('url') == "viewer_true"
           || $.mobile.activePage.jqmData('url') == "viewer_false") {
      if(e.shiftKey == true || e.ctrlKey == true) {
        // 前ページ
        if(e.which == 32) {
          if(pdfviewer.mode == "comic" || pdfviewer.mode == "tate" || pdfviewer.mode == "yoko") {
            if(pdfviewer.page > 1) {
              var plane = !viewer_plane;
              var dir = (pdfviewer.mode == "yoko") ? true : false;
              load_page(plane,true,-1,function() {
                $.mobile.changePage("#viewer_"+plane,{
                  transition:"<%=PAGE_EFFECT%>",
                  reverse:dir
                  // changeHash:false
                });
              });
            }
          } else {
            if(pdfviewer.page > 2) {
              var plane = !viewer_plane;
              var dir = (pdfviewer.mode == "yoko2") ? true : false;
              load_page(plane,true,-2,function() {
                $.mobile.changePage("#viewer_"+plane,{
                  transition:"<%=PAGE_EFFECT%>",
                  reverse:dir
                  // changeHash:false
                });
              });
            }
          }
        }
      } else {
        // 次ページ
        if(e.which == 32) {
          if(pdfviewer.mode == "tate") {
            if(pdfviewer.page < pdfviewer.pages) {
              var plane = !viewer_plane;
              load_page(plane,false,1,function() {
                $.mobile.changePage("#viewer_"+plane,{
                  transition:"<%=PAGE_EFFECT%>",
                  reverse:true
                  // changeHash:false
                });
              });
            }
          } else if(pdfviewer.mode == "tate2" || pdfviewer.mode == "yoko2") {
            if(pdfviewer.page < pdfviewer.pages) {
              var plane = !viewer_plane;
              var dir = (pdfviewer.mode == "yoko2") ? false : true;
              load_page(plane,false,2,function() {
                $.mobile.changePage("#viewer_"+plane,{
                  transition:"<%=PAGE_EFFECT%>",
                  reverse:dir
                  // changeHash:false
                });
              });
            }
          } else {
            var viewer = "#viewer_content_"+viewer_plane;
            var image = "#viewer_img_"+viewer_plane;
            if(($(viewer).scrollTop() + $(window).height()) >= $(image).height()) {
              if(pdfviewer.page < pdfviewer.pages) {
                var plane = !viewer_plane;
                var dir = (pdfviewer.mode == "yoko") ? false : true;
                load_page(plane,false,1,function() {
                  $.mobile.changePage("#viewer_"+plane,{
                    transition:"<%=PAGE_EFFECT%>",
                    reverse:dir
                    // changeHash:false
                  });
                });
              }
             } else {
               $(viewer).animate({
                 scrollTop:$(image).height() - $(window).height() + 6
               },"fast");
             }
          }
        }
      }
    // --------------------
    // 検索ダイアログの実行
    // --------------------
    } else if($.mobile.activePage.jqmData('url') == "find_dialog") {
      if(e.which == 13) {
        $("#find_dialog_exec").click();
      }
    }
  });
});

#### main

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title><%=TITLE%></title>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <!-- meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=4.0, minimum-scale=1.0" / -->
    <meta name="viewport" content="user-scalable=yes, initial-scale=1.0, maximum-scale=4.0, minimum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />
    <meta name="format-detection" content="no" />
    <meta name="msapplication-TileImage" content="./images/rodeo-thumbnail144.png"/>
    <meta name="msapplication-TileColor" content="#d83434"/>
    <meta name="apple-mobile-web-app-title" content="電子化viewer2">
    <link rel="apple-touch-icon" href="./images/apple-touch-icon.png" />
    <%if(req['user-agent'] =~ /iphone/i)%>
      <%if(false)%>
        <link rel="apple-touch-startup-image" href="./images/rodeo-iphone5.png" />
      <%else%>
        <link rel="apple-touch-startup-image" href="./images/rodeo-iphone.png" />
      <%end%>
    <%end%>
    <%if(req['user-agent'] =~ /ipad/i)%>
      <link rel="apple-touch-startup-image" href="./images/rodeo-ipad-port.png" media="screen and (orientation:portrait)"  />
      <link rel="apple-touch-startup-image" href="./images/rodeo-ipad-land.png" media="screen and (orientation:landscape)" />
    <%end%>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.css" />
    <link rel="stylesheet" href="?action=rodeo.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="?action=jqminit.js"></script>
    <script src="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.js"></script>
    <script src="?action=rodeo.js&slave=<%=rodeo['slave']%>&return=<%=rodeo['return']%>&where=<%=rodeo['where']%>"></script>
  </head>
  <body id="body" style="background-color: black;">
    <%rodeo.eruby(ERB_FILE,"dummy",                  binding)%>
    <%rodeo.eruby(ERB_FILE,"login",                  binding)%>
    <!-- 書籍一覧 -->
    <%flag = "true";  rodeo.eruby(ERB_FILE,"record", binding)%>
    <%flag = "false"; rodeo.eruby(ERB_FILE,"record", binding)%>
    <!-- ビューアー -->
    <%flag = "true";  rodeo.eruby(ERB_FILE,"viewer", binding)%>
    <%flag = "false"; rodeo.eruby(ERB_FILE,"viewer", binding)%>
    <!-- ダイアログ -->
    <%rodeo.eruby(ERB_FILE,"usr_config",             binding)%>
    <%rodeo.eruby(ERB_FILE,"find_dialog",            binding)%>
    <%rodeo.eruby(ERB_FILE,"record_menu",            binding)%>
    <%rodeo.eruby(ERB_FILE,"viewer_dialog",          binding)%>
    <%rodeo.eruby(ERB_FILE,"app_info",               binding)%>
  </body>
</html>

#### dummy

<section id="dummy" data-role="page" data-update-page-paddinge="false">
  <%if(rodeo["slave"] != "yes" and rodeo["return"] != "yes")%>
    <%if   (req['user-agent'] =~ /iphone/i)%>
      <img id="splash_image" src="./images/rodeo-iphone.png">
    <%elsif(req['user-agent'] =~ /android/i)%>
      <img id="splash_image" src="./images/rodeo-iphone.png">
    <%elsif(req['user-agent'] =~ /ipad/i)%>
      <img id="splash_image" src="./images/rodeo-ipad-port.png">
    <%else%>
      <img id="splash_image" src="./images/favicon.png"
       style="position: absolute; top: 50%; left: 50%; width: 191px; height: 191px; margin-top: -95px; margin-left: -95px;">
    <%end%>
  <%end%>
</section>

#### login

<section id="login" data-role="page" data-fullscreen="true" data-update-page-paddinge="false">
  <%if(rodeo["slave"] == "yes")%>
    <%theme = SLAVE_THEME%>
  <%else%>
    <%theme = MAIN_THEME%>
  <%end%>
  <header id="login_header" data-role="header" data-theme="<%=theme%>" style="height: <%=TITLEBAR_SIZE%>px;">
    <h1><%=TITLE%></h1>
    <div class="ui-btn-left">
      <a href="#app_info" data-role="button" data-icon="info" data-mini="true" data-inline="true" data-iconpos="notext" data-transition="pop">情報</a>
    </div>
    <div class="ui-btn-right">
      <a href="#" id="login_button" data-role="button" data-icon="arrow-r" data-mini="true" data-inline="true" data-transition="<%=LOGIN_EFFECT%>">ログイン</a>
    </div>
  </header>
  <div id="login_content" data-role="content" data-theme="<%=SUB_THEME%>">
    <form id="account" action="<%=APP_NAME%>" method="post">
      <input type="hidden" name="action" value="login">
      <ul data-role="listview" data-inset="true">
        <li>
          <div data-role="fieldcontain">
            <label for="userid" style="vertical-align: middle;">ユーザID</label>
            <input id="userid" type="email" name="userid" placeholder="メールアドレス" value="<%=rodeo['userid']%>">
          </div>
        </li>
        <li>
          <div data-role="fieldcontain">
            <label for="passwd" style="vertical-align: middle;">パスワード</label>
            <input id="passwd" type="password" name="passwd" placeholder="パスワード" value="">
          </div>
        </li>
      </ul>
    </form>
  </div>
</section>

#### record

<section id="record_<%=flag%>" data-role="page" data-fullscreen="true" data-update-page-paddinge="false">
  <%iconpos = %Q( data-iconpos="notext" )%>
  <%top = %Q(top: 0px;)%>
  <%#if(req['user-agent'] !~ /ipad/i and req['user-agent'] !~ /iphone/i and req['user-agent'] !~ /android/i)%>
    <%#iconpos = ""%>
    <%#top = %Q(top: -3px;)%>
  <%#end%>
  <%if(rodeo["slave"] == "yes")%>
    <%theme = SLAVE_THEME%>
  <%else%>
    <%theme = MAIN_THEME%>
  <%end%>
  <header id="record_header_<%=flag%>" data-role="header" data-theme="<%=theme%>" style="height: <%=TITLEBAR_SIZE%>px;">
    <h1 id="record_table_name_<%=flag%>"></h1>
      <div class="ui-btn-left">
        <%if(rodeo["slave"] == "yes")%>
          <a href="#" id="close_button_<%=flag%>"  data-role="button" data-icon="back"     data-mini="true" data-inline="true" <%=iconpos%>>戻る</a>
        <%else%>
          <a href="#" id="logout_button_<%=flag%>" data-role="button" data-icon="delete"   data-mini="true" data-inline="true" <%=iconpos%>>終了</a>
          <%if(req['user-agent'] !~ /iphone/i and req['user-agent'] !~ /android/i)%>
            <a href="#app_info"                    data-role="button" data-icon="info"     data-mini="true" data-inline="true" data-iconpos="notext" data-transition="pop">情報</a>
          <%end%>
          <a href="#usr_config"                    data-role="button" data-icon="gear"     data-mini="true" data-inline="true" <%=iconpos%> data-rel="dialog" data-transition="pop" title="パスワード変更">PW</a>
        <%end%>
        <%if(rodeo["slave"] != "yes" and req['user-agent'] !~ /ipad/i and req['user-agent'] !~ /iphone/i)%>
          <a href="#" id="new_window_<%=flag%>"    data-role="button" data-icon="newlist"  data-mini="true" data-inline="true" <%=iconpos%> rel="external" title="一時一覧モードに移行する">一時</a>
          <!--
          <a href="#" id="new_twitter_<%=flag%>"   data-role="button" data-icon="twitter"  data-mini="true" data-inline="true" <%=iconpos%> rel="external" title="Twitterを\n別ウインドウで開く">T</a>
          <a href="#" id="new_facebook_<%=flag%>"  data-role="button" data-icon="facebook" data-mini="true" data-inline="true" <%=iconpos%> rel="external" title="Facebookを\n別ウインドウで開く">F</a>
          -->
        <%end%>
      </div>
      <div class="ui-btn-right">
        <a href="#find_dialog" id="record_find_dialog_<%flag%>" data-role="button" data-icon="search"  data-mini="true" data-inline="true" <%=iconpos%> data-rel="dialog" data-transition="pop" title="一覧の条件を設定する">条件</a>
        <%if(req['user-agent'] !~ /iphone/i and req['user-agent'] !~ /android/i)%>
          <a href="#" id="record_refresh_btn_<%=flag%>"         data-role="button" data-icon="refresh" data-mini="true" data-inline="true" data-iconpos="notext" data-transition="fade" title="一覧表示を更新する">更新</a>
        <%end%>
        <a href="#" id="record_prev_btn_<%=flag%>"              data-role="button" data-icon="arrow-l" data-mini="true" data-inline="true" data-iconpos="notext" data-transition="<%=INDEX_EFFECT%>" data-direction="reverse" title="前の一覧表示">前</a>
        <a href="#" id="record_next_btn_<%=flag%>"              data-role="button" data-icon="arrow-r" data-mini="true" data-inline="true" data-iconpos="notext" data-transition="<%=INDEX_EFFECT%>" title="次の一覧表示">次</a>
      </div>
  </header>
  <div id="record_content_<%=flag%>" data-role="content" data-theme="<%=SHELF_THEME%>"
    style="width: 100%; padding-top: -1px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
  </div>
</section>

#### viewer

<section id="viewer_<%=flag%>" data-role="page" data-fullscreen="true" data-update-page-paddinge="false" style="margin: 0px; padding: 0px;">
  <div id="viewer_content_<%=flag%>" data-role="content" data-theme="<%=MAIN_THEME%>"
    style="background-color: gray; margin: 0px; padding: 0px; text-align: left;">
    <!-- 次ページボタン --->
    <div id="viewer_next_div_<%=flag%>" class="ui-btn-corner-all ui-shadow"
      style="position: fixed; border: solid; border-width: 1px; background-color: lightgray; left: 0px; top: -1px; width: 64px; height: 64px; opacity: <%=BTN_OPACITY%>">
      <a href="#" id="viewer_next_area_<%=flag%>" data-role="none" title="次のページへ移動する">
        <img id="viewer_next_img_<%=flag%>" src="./images/rodeo-prev.png" style="width: 64px; height: 64px;">
      </a>
    </div>
    <!-- 前ページボタン --->
    <div id="viewer_prev_div_<%=flag%>" class="ui-btn-corner-all ui-shadow"
      style="position: fixed; border: solid; border-width: 1px; background-color: lightgray; left: 0px; top: -1px; width: 64px; height: 64px; opacity: <%=BTN_OPACITY%>">
      <a href="#" id="viewer_prev_area_<%=flag%>" data-role="none" title="前のページへ移動する">
        <img id="viewer_prev_img_<%=flag%>" src="./images/rodeo-next.png" style="width: 64px; height: 64px;">
      </a>
    </div>
    <!-- 戻るボタン --->
    <div id="viewer_back_div_<%=flag%>" class="ui-btn-corner-all ui-shadow"
      style="position: fixed; border: solid; border-width: 1px; background-color: lightgray; left: 0px; top: -1px; width: 64px; height: 64px; opacity: <%=BTN_OPACITY%>">
      <a href="#" id="viewer_back_area_<%=flag%>" data-role="none" title="書籍一覧に戻る">
        <img id="viewer_back_img_<%=flag%>" src="./images/rodeo-bar.png" style="width: 64px; height: 64px;">
      </a>
    </div>
    <!-- 設定ボタン --->
    <div id="viewer_conf_div_<%=flag%>" class="ui-btn-corner-all ui-shadow"
      style="position: fixed; border: solid; border-width: 1px; background-color: lightgray; left: 0px; top: -1px; width: 64px; height: 64px; opacity: <%=BTN_OPACITY%>">
      <a href="#" id="viewer_conf_area_<%=flag%>" data-role="none" data-rel="dialog" data-transition="pop" title="読書環境を設定する">
        <img id="viewer_conf_img_<%=flag%>" src="./images/rodeo-tool.png" style="width: 64px; height: 64px;">
      </a>
    </div>
    <!-- 抽出中インジケータ --->
    <img id="viewer_loading_<%=flag%>" src="http://code.jquery.com/mobile/1.1.1/images/ajax-loader.gif"
      style="display: none; position: fixed; border-style: none; left: 0px; top: -1px; opacity: 0.4;">
    <!-- ルーペ --->
    <%if(req['user-agent'] =~ /iphone/i or req['user-agent'] =~ /android/i)%>
      <div id="viewer_loupe_<%=flag%>" style="
        display: none; position: fixed; left: 0px; top: -1px; width: <%=MOBILE_LOUPE_SIZE%>px; height: <%=MOBILE_LOUPE_SIZE%>px;
        background-repeat: no-repeat;
        background-color: gray;
        border: solid; border-width: 3px; border-color: gray;
        border-radius: <%=LOUPE_RADIUS%>px;
        -webkit-border-radius: <%=LOUPE_RADIUS%>px;
        -moz-border-radius: <%=LOUPE_RADIUS%>px;
        cursor: url('./images/rodeo-null.cur'),default;
        overflow: hidden;">
      </div>
    <%else%>
      <div id="viewer_loupe_<%=flag%>" style="
        display: none; position: fixed; left: 0px; top: -1px; width: <%=PC_LOUPE_SIZE%>px; height: <%=PC_LOUPE_SIZE%>px;
        background-repeat: no-repeat;
        background-color: gray;
        border: solid; border-width: 3px; border-color: gray; background-color: gray;
        border-radius: <%=LOUPE_RADIUS%>px;
        -webkit-border-radius: <%=LOUPE_RADIUS%>px;
        -moz-border-radius: <%=LOUPE_RADIUS%>px;
        cursor: url('./images/rodeo-null.cur'),default;
        overflow: hidden;">
      </div>
    <%end%>
    <!-- ページ -->
    <img id="viewer_img_<%=flag%>" style="margin: 0px; padding: 0px; border: solid 1px gray;">
  </div>
</section>

#### usr_config

<section id="usr_config" data-role="page" data-update-page-paddinge="false" data-close-text="キャンセル">
  <%if(rodeo["slave"] == "yes")%>
    <header data-role="header" data-theme="<%=SLAVE_THEME%>" style="height: 40px;">
  <%else%>
    <header data-role="header" data-theme="<%=MAIN_THEME%>"  style="height: 40px;">
  <%end%>
    <h1>パスワード変更</h1>
    <a href="#usr_config" id="usr_config_exec" data-icon="check" class="ui-btn-right" data-transition="pop">変更</a>
  </header>
  <div data-role="content" data-mini="true" data-theme="<%=SUB_THEME%>">
    <form id="config" action="<%=APP_NAME%>" method="post">
      <input type="hidden" name="action" value="passwd">
      <ul data-role="listview" data-inset="true">
        <li>
          <div data-role="fieldcontain">
            <label for="new_pass1">新パスワード</label>
            <input type="password" data-mini="true" name="new_pass1" id="new_pass1" value="">
          </div>
        </li>
        <li>
          <div data-role="fieldcontain">
            <label for="new_pass2">新パスワード(確認入力)</label>
            <input type="password" data-mini="true" name="new_pass2" id="new_pass2" value="">
          </div>
        </li>
      </ul>
    </form>
  </div>
</section>

#### record_menu

<section id="record_menu" data-role="dialog" data-update-page-paddinge="false" data-close-text="キャンセル">
  <%if(rodeo["slave"] == "yes")%>
    <header data-role="header" data-theme="<%=SLAVE_THEME%>" style="height: 40px;">
  <%else%>
    <header data-role="header" data-theme="<%=MAIN_THEME%>"  style="height: 40px;">
  <%end%>
    <h1>書籍情報</h1>
    <a href="#" id="record_menu_exec" data-icon="check" class="ui-btn-right" data-transition="pop">閉じる</a>
  </header>
  <div data-role="content" data-mini="true" data-theme="<%=SUB_THEME%>" style="padding-left: 15%; padding-right: 15%;">
    <fieldset data-role="controlgroup">
      <legend></legend>
      <h5 id="record_menu_title"  style="margin: 0px;"></h5>
      <h5 id="record_menu_author" style="margin: 0px;"></h5>
      <h5 id="record_menu_distr"  style="margin: 0px;"></h5>
      <h5 id="record_menu_isbn"   style="margin: 0px;"></h5>
      <h5 id="record_menu_status" style="margin: 0px;"></h5>
    </fieldset>
    <fieldset data-role="controlgroup">
      <legend></legend>
      <input type="checkbox" data-mini="true" name="record_menu_mark" id="record_menu_mark" value="true" />
      <label for="record_menu_mark">マーク</label>
    </fieldset>
    <fieldset data-role="controlgroup">
      <legend></legend>
      <a id="record_menu_cover" href="#" data-role="button" data-mini="true">
        <div><%=TITLE%>で読む</div>
        <img id="record_menu_image" style="width: <%=THUMBNAIL_WIDTH%>; border: solid 4px; border-color: gray;">
      </a>
    </fieldset>
    <div class="ui-grid-a">
      <div class="ui-block-a">
        <fieldset data-role="controlgroup" style="margin: 0px;">
          <legend></legend>
          <a id="record_menu_search" href="#" data-role="button" data-mini="true" data-icon="search">シリーズ</a>
        </fieldset>
      </div>
      <div class="ui-block-b">
        <fieldset data-role="controlgroup" style="margin: 0px;">
          <legend></legend>
          <a id="record_menu_author_search" href="#" data-role="button" data-mini="true" data-icon="search">著者</a>
        </fieldset>
      </div>
      <div class="ui-block-a">
        <fieldset data-role="controlgroup" style="margin: 0px;">
          <legend></legend>
          <a id="record_menu_download" href="#" data-role="button" data-mini="true" data-icon="arrow-d">Download</a>
        </fieldset>
      </div>
      <div class="ui-block-b">
        <fieldset data-role="controlgroup" style="margin: 0px;">
          <legend></legend>
          <a id="record_menu_amazon" href="#" data-role="button" data-mini="true" data-icon="amazon">Amazon</a>
        </fieldset>
      </div>
      <%if((req['user-agent'] !~ /iphone/i and req['user-agent'] !~ /ipad/i))%>
        <div class="ui-block-a">
          <fieldset data-role="controlgroup" style="margin: 0px;">
            <legend></legend>
            <a id="record_menu_twitter"  href="#" data-role="button" data-mini="true" data-icon="twitter" >Twitter</a>
          </fieldset>
        </div>
        <div class="ui-block-b">
          <fieldset data-role="controlgroup" style="margin: 0px;">
            <legend></legend>
            <a id="record_menu_facebook" href="#" data-role="button" data-mini="true" data-icon="facebook">Facebook</a>
          </fieldset>
        </div>
      <%end%>
    </div>
  </div>
</section>

#### find_dialog

<section id="find_dialog" data-role="dialog" data-update-page-paddinge="false" data-close-text="キャンセル">
  <%if(rodeo["slave"] == "yes")%>
    <header data-role="header" data-theme="<%=SLAVE_THEME%>" style="height: 40px;">
  <%else%>
    <header data-role="header" data-theme="<%=MAIN_THEME%>"  style="height: 40px;">
  <%end%>
    <h1>条件設定</h1>
    <a href="#" id="find_dialog_exec" data-icon="check" class="ui-btn-right" data-transition="pop">実行</a>
  </header>
  <div data-role="content"  data-mini="true" data-theme="<%=SUB_THEME%>">
    <!-- 検索条件 -->
    <div data-role="fieldcontain">
      <label for="find_dialog_qstring">表示条件</label>
      <input type="search" data-mini="true" name="qstring" id="find_dialog_qstring" value="" />
      <label for="find_dialog_qstring"></label>
      <select data-mini="true" name="search_history" id="search_history" data-native-menu="true">
        <option value="">--- 選択してください ---</option>
      </select>
    </div>
    <!-- 表示対象 -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup">
        <legend>一覧表示</legend>
        <label for="order_mark2">マーク済み</label>
        <input type="radio" data-mini="true" name="order_mark" id="order_mark2" value="on" />
        <label for="order_mark1">全ての書籍</label>
        <input type="radio" data-mini="true" name="order_mark" id="order_mark1" value="all" checked="checked" />
      </fieldset>
    </div>
    <!-- 並び替え項目 -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup">
        <legend>並び替え</legend>
        <input type="radio" data-mini="true" name="order_field" id="order_field2" value="name" />
        <label for="order_field2">　名前順　</label>
        <input type="radio" data-mini="true" name="order_field" id="order_field1" value="roid" checked="checked" />
        <label for="order_field1">　登録順　</label>
      </fieldset>
    </div>
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup">
        <legend></legend>
        <input type="radio" data-mini="true" name="order_dir" id="order_dir1" value="asc" checked="checked" />
        <label for="order_dir1">　昇順▲　</label>
        <input type="radio" data-mini="true" name="order_dir" id="order_dir2" value="desc"  />
        <label for="order_dir2">　降順▼　</label>
      </fieldset>
    </div>
  </div>
</section>

#### viewer_dialog

<section id="viewer_dialog" data-role="dialog" data-update-page-paddinge="false" data-close-text="キャンセル">
  <%if(rodeo["slave"] == "yes")%>
    <header data-role="header" data-theme="<%=SLAVE_THEME%>" style="height: 40px;">
  <%else%>
    <header data-role="header" data-theme="<%=MAIN_THEME%>"  style="height: 40px;">
  <%end%>
    <h1>表示設定</h1>
    <a href="#viewer" id="viewer_dialog_exec" data-icon="check" class="ui-btn-right">設定</a>
  </header>
  <div data-role="content" data-mini="true" data-theme="<%=SUB_THEME%>">
    <!-- ページ移動 -->
    <div data-role="fieldcontain">
      <label for="viewer_slider">ページ移動</label>
      <input type="range" data-mini="true" name="viewer_slider" id="viewer_slider" value="1" min="1" max="250" step="1" data-inline="true" />
    </div>
    <!-- カラー/グレースケール/白黒 -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup" data-type="horizontal">
        <legend>モード</legend>
        <label for="color-mode1">カラー</label>
        <input type="radio" data-mini="true" name="color-mode" id="color-mode1" value="color" />
        <label for="color-mode2">グレー</label>
        <input type="radio" data-mini="true" name="color-mode" id="color-mode2" value="gray" />
        <label for="color-mode3">白　黒</label>
        <input type="radio" data-mini="true" name="color-mode" id="color-mode3" value="mono" />
      </fieldset>
    </div>
    <!-- コントラスト -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup" data-type="horizontal" data-role="fieldcontain">
        <legend>コントラスト</legend>
        <label for="contrast-mode0"> 0</label><input type="radio" data-mini="true" name="contrast-mode" id="contrast-mode0" value="0" checked="checked" />
        <label for="contrast-mode1">+1</label><input type="radio" data-mini="true" name="contrast-mode" id="contrast-mode1" value="1" />
        <label for="contrast-mode2">+2</label><input type="radio" data-mini="true" name="contrast-mode" id="contrast-mode2" value="2" />
        <label for="contrast-mode3">+3</label><input type="radio" data-mini="true" name="contrast-mode" id="contrast-mode3" value="3" />
        <label for="contrast-mode4">+4</label><input type="radio" data-mini="true" name="contrast-mode" id="contrast-mode4" value="4" />
        <label for="contrast-mode5">+5</label><input type="radio" data-mini="true" name="contrast-mode" id="contrast-mode5" value="5" />
      </fieldset>
    </div>
    <!-- コミック/右開き/左開き -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup">
        <legend>読み方</legend>
        <label for="page-mode1">マンガ(スクロール有)</label>
        <input type="radio" data-mini="true" name="page-mode" id="page-mode1" value="comic" />
        <label for="page-mode2">マンガ・文庫</label>
        <input type="radio" data-mini="true" name="page-mode" id="page-mode2" value="tate" checked="checked" />
        <label for="page-mode3">マンガ・文庫見開き</label>
        <input type="radio" data-mini="true" name="page-mode" id="page-mode3" value="tate2" />
        <label for="page-mode4">ヨコ書き(スクロール有)</label>
        <input type="radio" data-mini="true" name="page-mode" id="page-mode4" value="yoko" />
        <label for="page-mode5">ヨコ書き見開き</label>
        <input type="radio" data-mini="true" name="page-mode" id="page-mode5" value="yoko2" />
      </fieldset>
    </div>
    <!-- トリミング -->
    <div data-role="fieldcontain">
      <label for="trim">余白削除(%)</label>
      <input type="range" data-mini="true" name="trim" id="trim" value="0" min="0" max="10" step="1" />
    </div>
    <!-- ページボタン -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup">
        <legend>ページ送り</legend>
        <label for="buttondir-mode">ページ送りボタンを左右逆転する</label>
        <input type="checkbox" data-mini="true" name="buttondir-mode" id="buttondir-mode" value="false" />
        <label for="swipe" >スワイプでページ送りをする</label>
        <input type="checkbox" data-mini="true" name="swipe" id="swipe" value="true" />
      </fieldset>
    </div>
    <!-- 自動傾斜補正 -->
    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup">
        <legend>補正機能</legend>
        <label for="deskew" >ページの傾きを自動補正する</label>
        <input type="checkbox" data-mini="true" name="deskew" id="deskew" value="false" />
        <label for="rotate" >横向き補正の方向を逆転する</label>
        <input type="checkbox" data-mini="true" name="rotate" id="rotate" value="true" />
      </fieldset>
    </div>
  </div>
</section>

#### facebook

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="msapplication-TileImage" content="./images/rodeo-thumbnail144.png"/>
    <meta name="msapplication-TileColor" content="#d83434"/>

    <meta property="og:title" content="<%=title%>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<%=url%>" />
    <meta property="og:image" content="<%=image%>" />
    <meta property="og:site_name" content="<%=TITLE%>" />

    <link rel="apple-touch-icon" href="./images/apple-touch-icon.png" />
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile.structure-1.1.1.min.css" />
    <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js"></script>
    <title><%=title%></title>
  </head>
  <body>
    <section id="amazon" data-role="page" data-close-text="閉じる">
      <header data-role="header" data-theme="<%=MAIN_THEME%>" style="height: 40px;">
        <h1><%=title%></h1>
      </header>
      <%if((req['user-agent'] =~ /iphone/i or req['user-agent'] =~ /android/i))%>
        <!-- Mobile -->
        <div data-role="content" data-mini="true" data-theme="<%=SUB_THEME%>">
          <div style="float: left;">
            <a href="http://www.amazon.co.jp/gp/aw/rd.html?ie=UTF8&__mk_ja_JP=%25E3%2582%25AB%25E3%2582%25BF%25E3%2582%25AB%25E3%2583%258A&at=<%=AMAZON_ASSOCID%>&k=<%=isbn%>&lc=mqr&m=stripbooks&uid=NULLGWDOCOMO&url=%2Fgp%2Faw%2Fs.html">
              <img border="0" src="<%=image%>" style="width: 200;">
              <div style="font-size: 80%; margin-top: 10px;">
                <img src="https://images-na.ssl-images-amazon.com/images/G/09/buttons/buy-from-tan.gif">
              </div>
            </a>
            <img src="http://www.assoc-amazon.jp/e/ir?t=<%=AMAZON_ASSOCID%>&l=mqr&o=9" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
          </div>
        </div>
      <%else%>
        <!-- PC -->
        <div data-role="content" data-mini="true" data-theme="<%=SUB_THEME%>">
          <a href="http://www.amazon.co.jp/gp/product/<%=asin%>/ref=as_li_tf_tl?ie=UTF8&camp=247&creative=1211&creativeASIN=<%=asin%>&linkCode=as2&tag=<%=AMAZON_ASSOCID%>">
            <div style="float: left; margin-right: 10px;">
              <img border="0" src="<%=image%>" style="width: 200;">
            </div>
            <div>
               <img src="https://images-na.ssl-images-amazon.com/images/G/09/buttons/buy-from-tan.gif">
            </div>
          </a>
          <div style="clear: both; padding-top: 10px;">
            <iframe src="http://rcm-jp.amazon.co.jp/e/cm?t=<%=AMAZON_ASSOCID%>&o=9&p=48&l=st1&mode=books-jp&search=<%=CGI.escape("#{author}")%>&fc1=000000&lt1=_top&lc1=3366FF&bg1=FFFFFF&f=ifr" marginwidth="0" marginheight="0" width="728" height="90" border="0" frameborder="0" style="border:none;" scrolling="no">
            </iframe>
          </div>
          <img src="http://www.assoc-amazon.jp/e/ir?t=<%=AMAZON_ASSOCID%>&l=as2&o=9&a=<%=asin%>" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
        </div>
      <%end%>
    </section>
  </body>
</html>

#### app_info

<section id="app_info" data-role="page" data-fullscreen="true" data-update-page-paddinge="false" style="margin: 0px; padding: 0px;">
  <style type="text/css">
    .title {
      font-weight: normal;
      color: SaddleBrown;
    }
  </style>
  <%if(rodeo["slave"] == "yes")%>
    <header data-role="header" data-position="fixed" data-theme="<%=SLAVE_THEME%>" data-tap-toggle="false" style="height: 40px;">
  <%else%>
    <header data-role="header" data-position="fixed" data-theme="<%=MAIN_THEME%>" data-tap-toggle="false" style="height: 40px;">
  <%end%>
    <h1><%=TITLE%>について</h1>
    <a href="#" data-rel="back" data-icon="back" data-direction="reverse">戻る</a>
  </header>
  <div data-role="content" data-theme="<%=SLAVE_THEME%>" style="margin-top: 40px;">
  <div style="padding: 5%; width: 90%; align: center;">

    <h2><span class="title"><%=TITLE%></span>作成記</h2>
    <p>無味乾燥な<span class="title"><%=TITLE%></span>の操作説明を書こうかと思ったけれど、
    ここは天邪鬼にエッセイ的な作成記を書いてみることにする(大体が、操作説明なんて一番読まれない文章だし(-_-))。</p>

    <h2>iPhone5でフルスクリーンで使う</h2>
    <p>待望のiPhone5が発売されたが、スクリーンサイズの違いに伴うmobile safariのアップデートでajaxがうまく動かなくなるなどのトラブルが発生している、
    「ホーム画面に追加」でスタンドアローン化すると従来のスクリーンサイズで表示されてしまう、というのもそのひとつ。回避策を探したら、</p>
    <div style="padding: 1em; border: dotted 2px gray;">
    data:text/html;charset=UTF-8,
    &lt;title&gt;<%=TITLE%>&lt;/title&gt;
    &lt;meta name=&quot;apple-mobile-web-app-capable&quot; content=&quot;yes&quot;&gt;
    &lt;link rel=&quot;apple-touch-icon&quot; href=&quot;http://dlsv.sgmail.jp/shokotan/images/apple-touch-icon2.png&quot;&gt;
    &lt;script&gt;
    if(window.navigator.standalone){location.href=&quot;http://dlsv.sgmail.jp/rvng&quot;;}
    else{document.write(&quot;ホーム画面に追加&quot;)}&lt;/script&gt;
    </div>
    <p>これをコピーしてURL欄に入れ、実行、表示された画面をホーム画面に追加すればいいらしい。<br>
    地図といい、mobile safariといい、もう少しなんとかならんのか ＞ Apple<br>
    ともあれ、なんとかフルスクリーンで使う方法が見つかって良かった。</p>

    <h2>Webアプリは楽ちんか？</h2>
    <p>Webアプリをいろいろ作ってきたけれど、年々Webアプリ作成の難易度が高くなってきているような気がする。<br>
    昔だったら、HTML、perlもしくはphpやrubyなどのスクリプト言語が使えればそれなりのものを作ることができた。<br>
    さらにもっと昔であれば(Webアプリじゃないけど)、Ｃ言語程度でアプリを作ることができた(Ｃ言語が簡単という意味ではない。あくまで技術シード数という意味)。<br>
    ところが、<span class="title"><%=TITLE%></span>の場合、
      <ul>
        <li>HTML5</li>
        <li>CSS3</li>
        <li>javascript</li>
        <li>Jquery</li>
        <li>Jquery mobile</li>
        <li>Ruby</li>
        <li>PostgreSQL</li>
        <li>Rodeo</li>
        <li>ImageMagick(RMagick)</li>
        <li>WEBrick</li>
      </ul>
    と、ざっと見ても１０の異なる言語やライブラリ、フレームワーク、ツールといったものを理解していないと個々の構成要素を書くことができなくなっている。<br>
    サーバ側のプログラムも、一昔前は、HTMLを生成できればそれでよかったけれど、今ではAjaxを使用したリクエストにjsonレスポンスを返すような実装をしないとスムーズな動作は期待できない。<br>
    確かに楽になった部分も多々あるけれど、複雑化していることは否めない。<br>
    技術は進歩しているのに、アプリを作るためのスキルは複雑化しているというのは一体どういうことなのか？<br>
    これは、それぞれの技術が互いに無関係に進歩してきたことが大きな理由だと思う。<br>
    ということで最近では、サーバサイドjavascriptといった話題を見かけることがある。これはWebアプリを開発する際の負担を少しでも少なくしようという動きの一つだ。<br>
    こういった状況から見ると、Webアプリは、楽ちんそうなイメージとは裏腹に、多くのスキルが必要なプロダクトなのである。</p>

    <h2>Ruby on Rails</h2>
    <p>少しプログラミングの世界を知っている人なら、なぜ上のスキルの中にRuby on Railsが入っていないのか、そして、見慣れないRodeoなる項目が入っているのか疑問を持つと思う。<br>
    そもそも、phpあたりからWebプログラミングを行なってきた人なら、上の一覧は、
      <ul>
        <li>HTML5</li>
        <li>CSS3</li>
        <li>javascript</li>
        <li>Jquery</li>
        <li>Jquery mobile</li>
        <li>PHP</li>
        <li>MySQL</li>
        <li>ImageMagick</li>
        <li>apache</li>
      </ul>
    という一覧になると思う。<br>
    答えは、Rubyが気に入っているし、Ruby on Railsより昔から作り、使ってきた、Rodeoのほうが使いやすいからにほかならない。<br>
    Ruby on Railsは優れたフレームワークだし、Railsが作られた頃、Rubyを学んでいたならためらわずRailsを使っていたのではないかと思う。<br>
    しかし、幸か不幸か、その頃すでに、Rodeoというフレームワーク兼データベースマネジメントツールを作ってしまっていたので、自然の流れとしてRodeoを使用しているというのが理由である。</p>

    <h2>Rodeo</h2>
    <p>Rodeoは、RubyからPostgreSQLをなるべく簡単に利用でき、かつ簡単にWebアプリを作るために作成したデータベース・マネジメントシステムである。<br>
    <span class="title"><%=TITLE%></span>にかぎらず、データベースを使用したアプリでは、アプリを作りながらデータベースの調整を行うことが多く、通常はSQLという言語を使用してデータベースの調整を行う。<br>
    また、Webアプリでデータベースの入出力を行う際にもSQL文を使用する。その際にRubyで扱えるように単なる文字列のデータからデータベースの型にあわせてデータの変換を行う必要がある。<br>
    RodeoはこのようないわゆるORマッパの機能の他にWebのフォーム要素を使用したWeb入出力機能を持ち、これらの機能を使用して作られたWebベースのデータベースマネージャを備えている。<br>
    ということで、少なくとも作者本人には便利な機能を備えたシステムである。</p>

    <h2>eRuby</h2>
    <p><span class="title"><%=TITLE%></span>を作るためには、eRubyと総称されるHTML構文中にRubyのプログラムを埋め込んで実行する機能が不可欠だ。使用しているeRubyはRodeoを作る際に作ったeRuby処理系で、Rubyプログラムの最後に__END__識別子を書き、その後にeRubyコードを書けるようになっている。加えて、入れ子が使えるようになっているので、長く煩雑になりかねないHTMLを分割して記述することが出来るようになっている。<br>
    これらの機能を使うことで、１つのプログラムファイルにRuby, css3, javascript, htmlを混在して記述することが可能になり、後述するWEBrickに組み込むことで、単一のプログラムファイルにアプリケーション部分を実装することができ、プログラム構成が非常にシンプルになる。</p>

    <h2><span class="title"><%=TITLE%></span>の構造</h2>
    <p>ということで、<span class="title"><%=TITLE%></span>はアプリケーション部分はひとつのテキストファイルになっている。<br>
    イメージとしては、<div style="padding-left: 1em; padding-right: 1em; border: dotted 2px gray;"><pre>
    #!/usr/bin/ruby -Ku
    {Rubyコード}
    __END__
    #### &lt;ブロック名&gt;
    {CSSコードブロック}
    #### &lt;ブロック名&gt;
    {Javascriptコードブロック}
    #### &lt;ブロック名&gt;
    {HTML5,eRubyコードブロック}
    </pre></div></p>
    <p>となっていて、Rubyコード以外は####行で区切られたコードブロックを書き、eRubyを使って各コードブロックを評価して出力する。<br>
    最初のeRuby呼び出しをRubyコード部分に書けば、呼び出されたコードブロック内でまたeRubyを使って他のブロックを内包できるようになっている。</p>

    <h2>Jquery mobile</h2>
    <p><span class="title"><%=TITLE%></span>を作る際にはJquery mobileの他にもいくつかのiPhone,iPad対応のライブラリを試してみた。結果、その中で一番使いやすかったのがJquery mobileだった。<br>
    また、Jquery mobileはiPhoneやiPad以外にも複数のプラットフォームに対応しているという点も大きい。</p>

    <h2>Imagemagick vs RMagick</h2>
    <p>PDFファイルから抽出したページは、そのままでは大きすぎたりするので画像処理を施す必要がある。<br>
    画像処理にはImagemagickを使っていたが、外部コマンドを呼び出す必要があり、もっとパフォーマンスの良い方法が必要になった。<br>
    そこでオンメモリーで画像処理の出来るRMagickを使うことにした。大量のメモリーを使用する代わり数倍レベルの高速化ができた。</p>

    <h2>FCGI,mod_ruby,WEBrick</h2>
    <p>快適な読書環境を提供するには画像処理部分の高速化はもちろん、その他の部分でもなるべく高速化したい。<br>
    方法としては、FCGI,mod_rubyがあるが、期待したほどの高速化はできなかった。<br>
    そこで、目先を変えてWEBrickを使ってみた。<br>
    FCGI,mod_rubyはプロセスレベルで高速化しようとするのに対して、WEBrickは高速化ではないけれど、スレッドレベルで各処理が動くのでプロセス生成のオーバーヘッドがなく結果的に高速化出来るようだ。<br>
    もちろん、ただ単にWEBrickからCGIを呼び出すようにしても効果はないので、<span class="title"><%=TITLE%></span>自体をWEBrickのサーバに組み込むことで高速化している。<br>
    こうすることで、Ajaxリクエストがスレッドとして動作しプロセスに比べてオーバーヘッドが少なくなる。<br>
    ただし、WEBrickを使った場合、すべての処理を１つのプロセス中で行うので、メモリーリークなどがあるとプロセスが肥大化したり、何らかのエラーでサーバ自体が落ちてしまう可能性があり、安定性に関しては検証する必要がある。<br>
    さらにapacheなどとの共存を考えると、80番ポート以外で動作させる必要があり、URLに余計な:10080などの記述が必要になる。このあたりは何らかの工夫が必要になる。<br>
    ということで、apacheにrewriteモジュールを組み込み、http://dlsv.sgmail.jp/rvngにアクセスした場合は、http://dlsv.sgmail.jp:10080/rvngに転送するように設定してある。<br>
    こんな細かな高速化を行なってどれだけになったかというと、初期には５～10秒かかっていたページ生成が２秒～５秒程度になっているので、ざっと２倍程度のパフォーマンスアップになっている。</p>

    <h2>WEBrick化の代償</h2>
    <p>WEBrickを使うことで高速化はできたが、そのかわり常駐するメモリー量が大きくなるという代償を受け入れる必要がある。<br>
    現時点で大体、500MBytes程度メモリーを消費するので、サーバのメモリーは少し多めにする必要がある。</p>

    <h2>Jquery mobileの高速化</h2>
    <p>サーバ側の高速化とあわせて、クライアント側(=Jquery mobile)も高速化が必要だ。<br>
    Jquery mobileにかぎらず、Ajaxを使ったアプリケーションはイベント駆動型になるため、イベント処理関連の部分を軽くしてやらないと全体的にもっさりとした動作に見えてしまう。<br>
    そのために、イベントが発生したらとりあえずイベントが発生したことを画面にフィードバックすることで、動作していることをユーザに通知する必要がある。<br>
    また、<span class="title"><%=TITLE%></span>で重要なのは次ページの先読み機能だ。次のページを予め読み込んでおくことで、次のページに遷移するときの待ち時間を短縮することができ、見かけ上スピードアップしたように見える。<br>
    意外と苦労したのは、次ページ遷移時の遷移エフェクトだ。ルーペ機能を実現するため、元画像は２倍の大きさで作っておき、表示する時は半分の大きさで表示するよう画像の大きさを50%表示にしていたのだが、こうするとなぜか次ページ遷移に時間がかかったり、引っかかったような動作になってしまう。<br>
    そのため表示用画像とルーペ用画像を別々に読みこむようにせざるを得なくなり、そのためルーペ機能(以前はretina.jsというライブラリを使用していた)自体をカスタムで作りなおすなどなど、ページ遷移をスムーズにするということだけで、解決には結構手間がかかっているのだ。</p>

    <h2>画像の扱い</h2>
    <p><span class="title"><%=TITLE%></span>はページ単位でPDFから画像を抽出してそれをクライアントに渡す。この時、ページ別のファイルを作成していると、あっという間に抽出した画像ファイルでハードディスクがいっぱいになってしまう、という事態になりかねない。<br>
    かといって、定期的にクリーンナップするような処理を組み込むのも削除するタイミングがつかみにくく厄介である。<br>
    ということで、<span class="title"><%=TITLE%></span>では、最終生成ページは同じファイル名を使う、という通常では余りやらない荒業を使用している。<br>
    もちろん、なんの工夫もなくこんなことをすれば、画像処理はめちゃくちゃになって、まともに文章を読むことは出来ないし、先読み機能を実装することも不可能だ。<br>
    そのため、<span class="title"><%=TITLE%></span>では、確実に画像を読み込んだあとに、次のページの画像を生成するようにタイミングをコントロールしている。なにもそこまで、と思うかもしれないけれど。</p>

    <h2>gridの数(Jquery Mobile)</h2>
    <p>Jquery mobileには、cssを使ったgridという擬似テーブルが使えるようになっているのだが、最大横5gridという制限がある。<br>
    使うとすればカレンダー表示が多いように思うのだが、5gridではカレンダーすら作れないというのは理解に苦しむ。<br>
    <span class="title"><%=TITLE%></span>でもPCで表示した場合、適度な密度でサムネイルを表示しようとすると5gridでは表示がスカスカになってしまう。<br>
    ということで、CSSを追加・変更し、7gridまで使えるように拡張している。出来れば本家で10grid程度まで扱えるようにしてもらえると助かるのだが。</p>

    <h2>なぜ<span class="title"><%=TITLE%></span>を作ったか？</h2>
    <p>PDFファイルを読む場合、Adobe readerなり、iPadならGoodreaderなどで読むのが普通だと思う。何を好き好んでこんなものを作ったのか？不思議に思う人も多いはずだ。<br>
    実際にスキャンしたPDFファイルを読んでみればわかることだが、スキャンしたPDFファイルはコントラストが低かったり、ページが傾いていたり、黄ばみが強かったりと、そのままでは読むに耐えないものがかなりあることが分かる。<br>
    そんなのは、クレームを付けてもっときちんとスキャンしてもらえばいい、という意見もあると思うが、スキャン品質にも限界というものがあり、どうやっても満足できない場合もあり得る。<br>
    こういう時に適当な補正機能を持ったPDFリーダーがあればいいのだが、そういった機能を持ったPDFリーダーはあまり存在しない。<br>
    加えて、iPadやAndroid端末の容量の問題もある。スキャンしたPDFファイルはデータ量が多く、一冊あたり100MBytes程度のファイルサイズになる。<br>
    iPadの場合、16GBytesのメモリーがフルに使えたとしても160冊で使いきってしまうことになる。テキストのみの場合と比較すると数十分の１の冊数しか持ち歩けない。<br>
    新しいiPadが出た場合はどうするか？古いiPadから数十GBytesのファイルの引越しが必要になるし、複数の端末でPDFファイルを読んでいたらいろいろ操作しているうち破損したりなくなってしまうPDFファイルも出てくる。<br>
    一度スキャンしたら安心して複数の端末で読める環境があれば、ファイルのコピーなどに長時間費やす必要もなく、無駄な作業をしなくて良いはずだ。<br>
    という、理由で作ったのが<span class="title"><%=TITLE%></span>というわけである。<br>
    もちろん、欠点もある。まず、高速な通信環境が必要だ。Wi-Fi+光インターネット環境が望ましいと思う。3Gオンリーだとかなり遅く感じるかもしれない。ダウンロードした場合と比べればページ送りも遅いことは否めない。<br>
    現時点では、電子化ドット・コムの追加サービスなので、電子化でスキャンしたファイル以外は読めない(これを欠点というかどうかは見方にもよるけど^^;)。</p>

    <h2><span class="title"><%=TITLE%></span>の便利なところ</h2>
    <p>とりあえず、列挙すると<ul>
      <li>ダウンロードせずに読める</li>
      <li>複数の端末で読み続けることが出来る</li>
      <li>どこまで読んだかが一覧に表示される</li>
      <li>簡単な絞り込み機能がついている</li>
      <li>TwitterやFacebookにつぶやける</li>
      <li>アマゾンの対象書籍のページをすぐに開ける</li>
      <li>ページをグレースケール／白黒化して読める</li>
      <li>ページの美白化やコントラストを変更して読める</li>
      <li>自動傾き補正機能がある</li>
      <li>余白の多い本をトリミングして読める</li>
      <li>簡単なお気に入りマーク機能がある</li>
      <li>５種類の読書モードがある</li>
      <li>フルスクリーンで読める</li>
      <li>ルーペ機能がある</li>
      <li>タブレット端末ではスワイプでページめくりが出来る</li>
      <li>Google Chromeに特化した機能を持つ</li>
    </ul>といったところか。<br>
    もちろん、最初からこれらの機能を備えていたわけではなく、プロトタイプがありそこから現在の形になっている。もちろんソフトウエアとしての寿命にならなければ今後も改良されていくことになる。</p>
    <p></p>

    <h2>Ruby</h2>
    <p>Rubyを使い始めたのは随分昔になる。確か、Vrersion 0.62(ちなみに今はversion 1.9.3が最新)とかの頃で、要するにベータ版以前から使っていることになる。オブジェクト指向だからとかそういった理由ではなく、単純に「Tkが扱えて日本語入力がまともに出来る」言語だったからという理由で使い始めた。<br>
    Ruby on Railsが出るまではそれほどメジャーな言語ではなく、どうやって普及させるか腐心していたくらいである。</br>
    最新版は1.9.3だが、<span class="title"><%=TITLE%></span>は1.8.7を使っている。理由は1.9系でruby-pgというpostgreSQLを利用するためのライブラリがきちんと動かなかったからだが、そろそろ1.9系を使っても大丈夫かもしれないので、様子をみて1.9系に乗り換えるかもしれない。</p>

    <h2>謎の空白ページ</h2>
    <p>PDFファイルからページを抽出する時、pdfimagesというプログラムを使用しているが、スキャンしたPDFファイルを展開した時"img-034.pbm"のような名前の1pxのpbmファイルを生成することがある。通常スキャンしたページは一つの画像で構成されているはずなのだが、どうやらスキャナーのPDFファイル生成ソフトが余分にファイルを生成しているらしい。<br>
    Adobe readerなどのPDFビューアで見る場合には何ら問題はないが、pdfimagesで抽出する場合、連番ファイルの一部にpbmファイルが混在するので、連番の番号とページ番号が対応しなくなってしまい、余計な空白ページができてしまったりする。<br>
    ということで、pdfimagesでページイメージを展開したあと、余計なpbmファイルを消去して連番を振り直す処理を行う必要があるのだが、実はページの表示を少しでも早くするため、pdfimagesの展開はバックグラウンドで行い、必要なページまで展開し終わったら、すぐにページの表示になるようにしているため、最初にページ番号がずれていると、タイミングによって、謎の空白ページを表示してしまうことがあり得る。<br>
    ページの表示を早くするか、正確性を優先するかの二律背反の状態が発生していて、現バージョンではページ表示を優先しているが、場合によっては正確性を優先するかもしれない。<br>
    また、pdfimagesにも少々問題があり、jpegファイルを生成するように-jオプションを指定しているにも関わらす、先の空白ページや稀にppmファイルを生成してしまうことがある。つまり、pdfimagesで２０ページ目を展開した時、img-20.jpg,img-20.ppm,img-20.ppmのどれかが生成されているということになり、どのファイルがあるかを確認してから読み込む必要があるという、ややっこしいことになっている。<br>では、ppmファイルを生成するようにしたら、ということでやってみるとppmファイルはjpegファイルに比べてファイルサイズが大きく、イメージ読み込みに時間がかかるようになってしまい、あまり良い結果にはならなかった。</p>

  </div>
  </div>
</section>
